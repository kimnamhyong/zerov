import React from "react";
import HeadComponent from "../../component/HeadComponent";
import UserMenuComponent from "../../component/UserMenuComponent";
import MenuComponent from "../../component/MenuComponent";
import SignupComponent from "../../component/user/SignupComponent";
import {Container} from "@mui/material";
import CopyrightComponent from "../../component/CopyrightComponent";

const Index = () => {
    return (
        <div>
            <HeadComponent title='회원가입'/>
            {/* 회원 관련 메뉴 시작*/}
            <UserMenuComponent user=''/>
            {/* 회원 관련 메뉴 끝*/}
            {/* 메뉴 컴포넌트 시작 */}
            <MenuComponent user='' title='회원가입'/>
            {/* 메뉴 컴포넌트 끝*/}
            {/* 회원가입 시작 */}
            <Container>
                <SignupComponent/>
            </Container>
            <CopyrightComponent/>
        </div>
    );
}


export default Index;