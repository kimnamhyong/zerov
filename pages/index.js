import React from "react";
import HeadComponent from "../component/HeadComponent";
import UserMenuComponent from "../component/UserMenuComponent";
import MenuComponent from "../component/MenuComponent";
import MainSwiperComponent from "../component/main/MainSwiperComponent";
import {Box, Container} from "@mui/material";
import MainServiceComponent from "../component/main/MainServiceComponent";
import MainContactComponent from "../component/main/MainContactComponent";
import CopyrightComponent from "../component/CopyrightComponent";

const Index = () => {
    return (
        <div>
            <HeadComponent title='제로브이'/>
            {/* 회원 관련 메뉴 시작*/}
            <UserMenuComponent user=''/>
            {/* 회원 관련 메뉴 끝*/}
            {/* 메뉴 컴포넌트 시작 */}
            <MenuComponent user='' title='제로브이'/>
            {/* 메뉴 컴포넌트 끝*/}
            {/* 메인화면 스와이퍼 시작 */}
            <MainSwiperComponent/>
            {/* 메인화면 스와이퍼 끝 */}
            {/* 메인화면 서비스 안내 시작*/}
            <MainServiceComponent/>
            {/* 메인화면 서비스 안내 끝*/}
            {/* 메인 연락 안내 시작 */}
            <MainContactComponent/>
            {/* 메인 연락 안내 끝*/}
            {/* 카피라이터 시작*/}
            <CopyrightComponent/>
            {/* 카피라이터 끝*/}
        </div>
    );
}


export default Index;