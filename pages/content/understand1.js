import React from "react";

import HeadComponent from "../../component/HeadComponent";
import UserMenuComponent from "../../component/UserMenuComponent";
import MenuComponent from "../../component/MenuComponent";
import {Container} from "@mui/material";
import CopyrightComponent from "../../component/CopyrightComponent";
import Service1Component from "../../component/content/Service1Component";
import UnderStand1Component from "../../component/content/UnderStand1Component";

const UnderStand1 = () => {
    return (
        <div>
            <HeadComponent title='저장강박과 자기방임'/>
            {/* 회원 관련 메뉴 시작*/}
            <UserMenuComponent user=''/>
            {/* 회원 관련 메뉴 끝*/}
            {/* 메뉴 컴포넌트 시작 */}
            <MenuComponent user='' title='저장강박과 자기방임'/>
            {/* 메뉴 컴포넌트 끝*/}
            {/* 회원가입 시작 */}

            <UnderStand1Component/>

            <CopyrightComponent/>
        </div>
    )
}

export default UnderStand1;