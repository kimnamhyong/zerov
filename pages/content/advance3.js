import React from "react";

import HeadComponent from "../../component/HeadComponent";
import UserMenuComponent from "../../component/UserMenuComponent";
import MenuComponent from "../../component/MenuComponent";
import {Container} from "@mui/material";
import CopyrightComponent from "../../component/CopyrightComponent";
import Service1Component from "../../component/content/Service1Component";
import Adance1Component from "../../component/content/Adance1Component";
import Adance3Component from "@/component/content/Adance3Component";

const Advance3 = () => {
    return (
        <div>
            <HeadComponent title='사전 교육 및 안전관리'/>
            {/* 회원 관련 메뉴 시작*/}
            <UserMenuComponent user=''/>
            {/* 회원 관련 메뉴 끝*/}
            {/* 메뉴 컴포넌트 시작 */}
            <MenuComponent user='' title='사전 교육 및 안전관리'/>
            {/* 메뉴 컴포넌트 끝*/}
            <Adance3Component/>
            <CopyrightComponent/>
        </div>
    )
}

export default Advance3;