import React from "react";

import HeadComponent from "../../component/HeadComponent";
import UserMenuComponent from "../../component/UserMenuComponent";
import MenuComponent from "../../component/MenuComponent";
import CopyrightComponent from "../../component/CopyrightComponent";
import Adance2Component from "@/component/content/Adance2Component";

const Advance2 = () => {
    return (
        <div>
            <HeadComponent title='서비스 준비 절차'/>
            {/* 회원 관련 메뉴 시작*/}
            <UserMenuComponent user=''/>
            {/* 회원 관련 메뉴 끝*/}
            {/* 메뉴 컴포넌트 시작 */}
            <MenuComponent user='' title='서비스 준비 절차'/>
            {/* 메뉴 컴포넌트 끝*/}
            <Adance2Component/>
            <CopyrightComponent/>
        </div>
    )
}

export default Advance2;