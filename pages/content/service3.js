import React from "react";

import HeadComponent from "../../component/HeadComponent";
import UserMenuComponent from "../../component/UserMenuComponent";
import MenuComponent from "../../component/MenuComponent";
import {Container} from "@mui/material";
import CopyrightComponent from "../../component/CopyrightComponent";
import Service3Component from "../../component/content/Service3Component";

const Service3 = () => {
    return (
        <div>
            <HeadComponent title='서비스 절차'/>
            {/* 회원 관련 메뉴 시작*/}
            <UserMenuComponent user=''/>
            {/* 회원 관련 메뉴 끝*/}
            {/* 메뉴 컴포넌트 시작 */}
            <MenuComponent user='' title='서비스 절차'/>
            {/* 메뉴 컴포넌트 끝*/}
            {/* 회원가입 시작 */}

            <Service3Component/>

            <CopyrightComponent/>
        </div>
    )
}

export default Service3;