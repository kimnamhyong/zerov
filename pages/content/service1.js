import React from "react";

import HeadComponent from "../../component/HeadComponent";
import UserMenuComponent from "../../component/UserMenuComponent";
import MenuComponent from "../../component/MenuComponent";
import {Container} from "@mui/material";
import CopyrightComponent from "../../component/CopyrightComponent";
import Service1Component from "../../component/content/Service1Component";

const Service1 = () => {
    return (
        <div>
            <HeadComponent title='서비스 목적'/>
            {/* 회원 관련 메뉴 시작*/}
            <UserMenuComponent user=''/>
            {/* 회원 관련 메뉴 끝*/}
            {/* 메뉴 컴포넌트 시작 */}
            <MenuComponent user='' title='서비스 목적'/>
            {/* 메뉴 컴포넌트 끝*/}
            {/* 회원가입 시작 */}

             <Service1Component/>

            <CopyrightComponent/>
        </div>
    )
}

export default Service1;