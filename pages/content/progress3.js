import React from "react";

import HeadComponent from "../../component/HeadComponent";
import UserMenuComponent from "../../component/UserMenuComponent";
import MenuComponent from "../../component/MenuComponent";
import {Container} from "@mui/material";
import CopyrightComponent from "../../component/CopyrightComponent";
import Service1Component from "../../component/content/Service1Component";
import Adance1Component from "../../component/content/Adance1Component";
import Progress3Component from "@/component/content/Progress3Component";

const Progress3 = () => {
    return (
        <div>
            <HeadComponent title='서비스 진행 단계'/>
            {/* 회원 관련 메뉴 시작*/}
            <UserMenuComponent user=''/>
            {/* 회원 관련 메뉴 끝*/}
            {/* 메뉴 컴포넌트 시작 */}
            <MenuComponent user='' title='서비스 진행 단계'/>
            {/* 메뉴 컴포넌트 끝*/}
            <Progress3Component/>
            <CopyrightComponent/>
        </div>
    )
}

export default Progress3;