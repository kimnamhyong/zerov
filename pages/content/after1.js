import React from "react";

import HeadComponent from "../../component/HeadComponent";
import UserMenuComponent from "../../component/UserMenuComponent";
import MenuComponent from "../../component/MenuComponent";
import {Container} from "@mui/material";
import CopyrightComponent from "../../component/CopyrightComponent";
import Service1Component from "../../component/content/Service1Component";
import Adance1Component from "../../component/content/Adance1Component";
import Progress1Component from "@/component/content/Progress1Component";
import After1Component from "@/component/content/After1Component";

const After1 = () => {
    return (
        <div>
            <HeadComponent title='사후 서비스 단계'/>
            {/* 회원 관련 메뉴 시작*/}
            <UserMenuComponent user=''/>
            {/* 회원 관련 메뉴 끝*/}
            {/* 메뉴 컴포넌트 시작 */}
            <MenuComponent user='' title='사후 서비스 단계'/>
            {/* 메뉴 컴포넌트 끝*/}
            <After1Component/>
            <CopyrightComponent/>
        </div>
    )
}

export default After1;