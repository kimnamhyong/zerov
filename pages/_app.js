import React from "react";
import PropTypes from "prop-types";
import Head from "next/head";
import '/public/css/main.css';
import '/public/css/swiper.css';
import '/public/css/menu.css';
import '/public/css/animation.css';
import { Hydrate, QueryClient, QueryClientProvider } from "react-query";//리액트 쿼리를 쓰려면 이게 필요함
import { ReactQueryDevtools } from "react-query/devtools";
import {motion} from "framer-motion";//페이지 전환시 애니메이션 효과 나타내기
import {useRouter} from "next/router";

//페이지 전환을 할 때 애니메이션 효과 css와 거의 비슷함
const variants = {
    enter: { opacity: 0, duration:'3s' },
    center: { opacity: 1,scale:1,duration:'3s' },
    exit: { opacity: 0, duration:'3s' },
};
const App = ({Component,pageProps}) => {
    const router = useRouter();
    const [queryClient] = React.useState(() => new QueryClient({
        defaultOptions:{
            queries: {
                retry: false,
            },
        }
    }));
    return (
        <>
            <Head>
                <title>앱</title>
            </Head>
            {/* 리액트 쿼리 세팅하기 */}
            <QueryClientProvider client={queryClient}>
                <Hydrate state={pageProps.dehydratedState}>
                    <motion.div
                        key={router.route}
                        initial="enter"
                        animate="center"
                        exit="exit"
                        variants={variants}
                    >
                        <Component {...pageProps}/>
                    </motion.div>
                </Hydrate>
                <ReactQueryDevtools/>
            </QueryClientProvider>
        </>
    );
}

App.propTypes = {
    Component: PropTypes.elementType.isRequired
}

export default App;