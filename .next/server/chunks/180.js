"use strict";
exports.id = 180;
exports.ids = [180];
exports.modules = {

/***/ 1266:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


const TabLayout = ({ index_no , onClickTab , tabMenu  })=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "tab-menu",
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("ul", {
                children: tabMenu.map((tab)=>{
                    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                        onClick: ()=>{
                            onClickTab(tab.id);
                        },
                        className: index_no === tab.id ? "active" : "",
                        children: tab.menu
                    }, tab.id);
                })
            })
        })
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TabLayout);


/***/ }),

/***/ 3180:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5692);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_mui_material__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_reveal_Bounce__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8223);
/* harmony import */ var react_reveal_Bounce__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_reveal_Bounce__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_reveal_Fade__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1665);
/* harmony import */ var react_reveal_Fade__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_reveal_Fade__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _component_TabLayout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1266);






const Adance1Component = ()=>{
    const tabMenu = [
        {
            id: 1,
            menu: "발굴"
        },
        {
            id: 2,
            menu: "욕구 및 조사"
        },
        {
            id: 3,
            menu: "사례회의"
        },
        {
            id: 4,
            menu: "구분 및 선정"
        },
        {
            id: 5,
            menu: "승인"
        }
    ];
    const [index_no, setIndexNo] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(1);
    const onClickTab = (no)=>{
        setIndexNo(no);
    };
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "service-content1 under1_back",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_2__.Container, {
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "title",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((react_reveal_Bounce__WEBPACK_IMPORTED_MODULE_3___default()), {
                                left: true,
                                children: [
                                    "사전 서비스 단계",
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "subtitle",
                                        children: "서비스 대상 가구 선정"
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_reveal_Bounce__WEBPACK_IMPORTED_MODULE_3___default()), {
                            bottom: true,
                            duration: 2000,
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "content",
                                children: [
                                    "서비스 대상 가구를 발굴과 욕구조사를 하며,",
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                    "정확한 원인 분석과 면담을 통해 가구를 선정합니다."
                                ]
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "service-content1-2",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_2__.Container, {
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_component_TabLayout__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                            index_no: index_no,
                            tabMenu: tabMenu,
                            onClickTab: onClickTab
                        }),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "content",
                            children: [
                                index_no === 1 ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "content-info2",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((react_reveal_Fade__WEBPACK_IMPORTED_MODULE_4___default()), {
                                        children: [
                                            "1) 대상자 접수",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 대상자 본인, 가족의 도움 요청",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 지역주민 신고: 통\xb7반장, 주민",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 공공기관 의뢰",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 민간기관 의뢰",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "2) 발굴 유형별 빈도",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 통상적인 발굴 유형별 빈도 비중은 지역주민 신고, 복지사 가정방문 발굴, 기관에 의한 의뢰 순으로 높게 나타남",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "3) 서비스 개입 유형별 빈도",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 발굴 가구에 대한 서비스 개입 유형별 빈도 비중은 주거환경 개선 서비스, 정신과 의뢰, 통합사례관리, 공적부조 신청, 기타 서비스 순으로 높게 나타남"
                                        ]
                                    })
                                }) : null,
                                index_no === 2 ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "content-info2",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((react_reveal_Fade__WEBPACK_IMPORTED_MODULE_4___default()), {
                                        children: [
                                            "욕구조사의 목적은 저장위기가구의 욕구 영역별 현상 및 원인을 파악하여 서비스제공계획을 수립하기 위한 기초자료로 활용하기 위함이다. 본 매뉴얼에서는 저장위기가구를 저장강박으로 인해 저장행동을 보이는 대상자와 자기방임으로 인해 저장행동을 보이는 대상자를 모두 포함함에 따라, 저장강박 척도와 자기방임 척도를 함께 사용하여 대상자의 저장행동의 원인을 분석하고 정서 및 행동의 종합적 판단을 통해 대상자를 정확히 진단하고 객관화한다.",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "1) 원인 분석",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 정서 진단 척도: 대상자의 저장강박 증상의 파악",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 저장강박 척도의 사용 (부록 1)",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "* 항목 예: ‘나는 물건을 버리는 것이 힘들고 고통스럽다’",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 행동 진단 척도: 대상자의 자기방임 증상 및 주거 생활환경의 불결상태의 파악",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 자기방임 척도의 사용 (부록 2)",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "* 항목 예: ‘나는 기본적인 위생과 청결에 최소한의 관리를 하지 않는다’",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 이미지 척도(Clutter Image Rating Scale)의 사용 (부록 3)",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "* 장소별(출입구 주변, 부엌, 거실, 침실, 욕실, 화장실, 베란다 등) 잡동사니가 쌓여있는 이미지 사진을 제시하여 대상자와 사례관리자가 개별적으로 저장행동 정도를 평가",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "2) 초기 면담 및 위기도 조사",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 초기 면담 및 관찰",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 대상자의 주된 문제와 전반적인 욕구 파악",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스 내용과 절차에 대한 안내",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스 기대 수준 확인",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스의 지속성과 책임성에 대한 신뢰감 형성",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 대상자와 협력적 관계 형성",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 추가 욕구조사",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 보다 정확한 서비스를 제공하기 위해 추가 척도를 활용한 욕구확인",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 위기도 조사",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 대상자의 욕구영역별 문제의 시급성과 중요성 파악",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 욕구영역별 위기도 평가 기준",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 면담자가 갖추어야 할 조건",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 대상자의 자발적인 참여를 유도하기 위한 상담 역량 및 전문성",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 유연성과 적극성을 가지고 대상자에게 접근",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {})
                                        ]
                                    })
                                }) : null,
                                index_no === 3 ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "content-info2",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((react_reveal_Fade__WEBPACK_IMPORTED_MODULE_4___default()), {
                                        children: [
                                            "1) 대상자 구분, 선정 및 대상자 가구에 대한 서비스제공계획 수립부터 서비스가 제공된 이후 대상자의 욕구 변화 및 문제해결 정도, 서비스 제공 점검, 사례관리자의 개입방법, 종결 및 점검 등을 협의하기 위하여 실시",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "2) 내부사례회의",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 대상자 가구선정, 서비스제공계획 수립, 점검, 종결 등을 협의",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 수행 주체: 사례관리자, 주민센터 담당자, 지자체(구청 등), 사회복지기관, 서비스 제공업체",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 수시 개최",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "* 대상자 선정과 종결시 필수 진행, 서비스 제공 중 필요시 수시 실시",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "3) 통합사례회의",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 주요 서비스 기관과 서비스 내용을 협의하여 연계방식을 조정",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 수행 주체: 사례관리자, 주민센터 담당자, 지자체(구청 등), 사회복지기관, 서비스 제공업체",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 관련 참여기관 및 참석자: 서비스 연계기관 담당자, 대상자 증상 및 질환 의료 전문가",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 정기적 실시",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "4) 사례회의 내용",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 대상자 욕구 해결을 위한 주요 강점",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 사례회의를 통해 해결할 수 있는 과제",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 사례회의를 통해 결정해야 할 서비스 제공 관련 내용",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 기존 지역내 공공-민간자원 활용방안",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {})
                                        ]
                                    })
                                }) : null,
                                index_no === 4 ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "content-info2",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((react_reveal_Fade__WEBPACK_IMPORTED_MODULE_4___default()), {
                                        children: [
                                            "1) 대상자 구분을 위한 준비",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 사례회의 결과를 바탕으로 대상자 가구 구분 및 선정을 위한 준비",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 초기 면담 내용과 욕구조사 내용 및 종합의견 확인",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 잠정적으로 대상자 구분하기",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "2) 대상자 구분 및 선정",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 욕구조사와 위기도 조사 결과를 토대로 서비스 제공 가구, 서비스 연계 가구, 서비스 미제공 가구로 구분",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스 제공 가구: 중장기적(1개월 이상) 개입이 필요한 가구. 다양한 서비스를 집중적으로 제공",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스 연계 가구: 단기적(30일 이내) 개입 가구. 단순 서비스 연계를 통해 문제해결 가능",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스 미제공 가구: 개입이 필요치 않은 가구 또는 대상자 연락 두절 및 거부 등으로 인해 기한 내에 욕구조사를 수행하지 못한 경우(미선정 처리 후 재신청 유도)",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 대상자에게 선정 결과를 통보하고 서비스 제공 가구에는 서비스에 대한 안내 시행",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "3) 서비스 제공 가구의 구분",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 일반 관리 가구",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 내부사례회의로 서비스계획수립이 가능한 가구",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 주거환경개선 서비스의 제공",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 위기 관리 가구",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 내부사례회의로 서비스계획수립이 가능한 가구",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 주거환경개선 서비스의 제공",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스 연계활동과 점검활동이 지속적으로 필요한 가구",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 집중 관리 가구",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 통합사례회의로 서비스계획수립이 가능한 가구",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 주거환경개선 서비스의 제공",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스 연계활동과 점검활동이 지속적으로 필요한 가구",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 일반/위기/집중 관리 가구의 구분 기준",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 대상자 지표: 저장강박(정서) 정도, 자기방임(행동) 정도, 기타 질환 등",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 이웃 지표: 민원 발생 횟수 등",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스 제공업체 지표: 폐기물량, 오염 정도 등",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "4) 서비스 제공을 위한 준비사항",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 서비스 제공 동의",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스 제공 가구 및 서비스 연계 가구에 필요한 서비스 내용 정리",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스제공자, 제공지원, 방법(제공횟수 및 시간) 등을 상세히 확인",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스 제공 내용 협의",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 서비스 이용 동의",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스 제공 및 서비스 연계 가구 대상자와 접촉하여 서비스 제공기관 등의 설명",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "- 서비스 이용 동의 구한 후 서비스 내용 최종 확정",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {})
                                        ]
                                    })
                                }) : null,
                                index_no === 5 ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "content-info2",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((react_reveal_Fade__WEBPACK_IMPORTED_MODULE_4___default()), {
                                        children: [
                                            "1) 서비스 제공업체와 사례관리자의 회의를 통해 서비스 제공 가구 사전 승인",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "2) 서비스 제공 및 서비스 연계 가구에 대한 서비스 제공계획 수립",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 대상자와 합의된 욕구 중심 장\xb7단기 목표 설정",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 기관 간 역할 분담",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "∙ 대상자 역할 규정",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                            "3) 최종 승인"
                                        ]
                                    })
                                }) : null
                            ]
                        })
                    ]
                })
            })
        ]
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Adance1Component);


/***/ })

};
;