"use strict";
exports.id = 883;
exports.ids = [883];
exports.modules = {

/***/ 5762:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


const CopyrightComponent = ()=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "copyright",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "copyright-content",
            children: [
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "compony-info",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "logo",
                            children: "제로브이 로고"
                        }),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "compony-infomation",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    children: "제로브이"
                                }),
                                "사업자등록번호 : 000-00-00000 | 부산광역시 해운대구 반송2동...",
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                "TEL : 1500-0000 | HP: 010-0000-0000 | E-mail : kkk@mail.net"
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "agree",
                    children: "이용약관 개인정보처리방침"
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "copyright-year",
                    children: "Copyright ⓒ 2023 제로브이 All rights reserved."
                })
            ]
        })
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CopyrightComponent);


/***/ }),

/***/ 3047:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);



const HeadComponent = ({ title  })=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_head__WEBPACK_IMPORTED_MODULE_2___default()), {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("title", {
            children: title
        })
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (HeadComponent);


/***/ }),

/***/ 1006:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ component_MenuComponent)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);
;// CONCATENATED MODULE: ./component/PcMenuComponent.js



const PcMenuComponent = ()=>{
    const [sub_no, setSubNo] = (0,external_react_.useState)(0);
    const onMouseOverSubMenu = (no)=>{
        setSubNo(no);
    };
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: "desktop-menu",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "left-menu",
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                src: "/images/logo.png",
                                width: "100"
                            })
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                            onMouseOver: ()=>{
                                onMouseOverSubMenu(1);
                            },
                            onMouseOut: ()=>{
                                setSubNo(0);
                            },
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                    href: "/content/service1",
                                    children: "서비스 개요"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: sub_no === 1 ? "submenu-open" : "submenu-close",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/service1",
                                                    children: "서비스 목적"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/service2",
                                                    children: "서비스 절차"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/service3",
                                                    children: "서비스 주요내용"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/service4",
                                                    children: "서비스 운영체제"
                                                })
                                            })
                                        ]
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                            onMouseOver: ()=>{
                                onMouseOverSubMenu(2);
                            },
                            onMouseOut: ()=>{
                                setSubNo(0);
                            },
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                    href: "/content/understand1",
                                    children: "저장행동의 이해"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: sub_no === 2 ? "submenu-open" : "submenu-close",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/understand1",
                                                    children: "저장강박과 자기방임"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/understand2",
                                                    children: "저장강박과 자기방임의 특성"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/understand3",
                                                    children: "저장위기가구의 진단"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/understand4",
                                                    children: "관련 주요 질환"
                                                })
                                            })
                                        ]
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                            onMouseOver: ()=>{
                                onMouseOverSubMenu(3);
                            },
                            onMouseOut: ()=>{
                                setSubNo(0);
                            },
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                    href: "/content/advance1",
                                    children: "사전 서비스 단계"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: sub_no === 3 ? "submenu-open" : "submenu-close",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/advance1",
                                                    children: "서비스 대상 가구 선정"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/advance2",
                                                    children: "서비스 준비 절차"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/advance3",
                                                    children: "사전 교육 및 안전관리"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/advance4",
                                                    children: "사전 준비물 점검"
                                                })
                                            })
                                        ]
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                            onMouseOver: ()=>{
                                onMouseOverSubMenu(4);
                            },
                            onMouseOut: ()=>{
                                setSubNo(0);
                            },
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                    href: "/content/progress1",
                                    children: "서비스 진행 단계"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: sub_no === 4 ? "submenu-open" : "submenu-close",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/progress1",
                                                    children: "서비스 유형별 절차"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/progress2",
                                                    children: "서비스 제공 단계별 실행 가이드"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/progress3",
                                                    children: "폐기물 처리"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/progress4",
                                                    children: "주거청소"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/progress5",
                                                    children: "소독\xb7방역"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/progress6",
                                                    children: "상황 발생에 대한 예방 및 관리대책"
                                                })
                                            })
                                        ]
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                            onMouseOver: ()=>{
                                onMouseOverSubMenu(5);
                            },
                            onMouseOut: ()=>{
                                setSubNo(0);
                            },
                            children: [
                                "사후 서비스 단계",
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: sub_no === 5 ? "submenu-open" : "submenu-close",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/after1",
                                                    children: "대상가구 교육 및 입주 관리"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/after2",
                                                    children: "서비스 종결 점검"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/after3",
                                                    children: "서비스 중단 및 재발에 대한 재개입 서비스"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                    href: "/content/after4",
                                                    children: "사후 지원 서비스"
                                                })
                                            })
                                        ]
                                    })
                                })
                            ]
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "right-menu"
            })
        ]
    });
};
/* harmony default export */ const component_PcMenuComponent = (PcMenuComponent);

// EXTERNAL MODULE: external "@mui/material"
var material_ = __webpack_require__(5692);
// EXTERNAL MODULE: external "@mui/material/IconButton"
var IconButton_ = __webpack_require__(7934);
var IconButton_default = /*#__PURE__*/__webpack_require__.n(IconButton_);
// EXTERNAL MODULE: external "@mui/icons-material/Menu"
var Menu_ = __webpack_require__(3365);
var Menu_default = /*#__PURE__*/__webpack_require__.n(Menu_);
// EXTERNAL MODULE: external "@mui/icons-material/KeyboardArrowRight"
var KeyboardArrowRight_ = __webpack_require__(547);
var KeyboardArrowRight_default = /*#__PURE__*/__webpack_require__.n(KeyboardArrowRight_);
// EXTERNAL MODULE: external "@mui/icons-material/KeyboardArrowDown"
var KeyboardArrowDown_ = __webpack_require__(4845);
var KeyboardArrowDown_default = /*#__PURE__*/__webpack_require__.n(KeyboardArrowDown_);
;// CONCATENATED MODULE: ./component/MobileMenuComponent.js








const MobileMenuComponent = ({ user , title  })=>{
    const [open, setOpen] = (0,external_react_.useState)(false);
    const [menuNo, setMenuNo] = (0,external_react_.useState)(0);
    //메뉴 클릭시
    const onClickMenu = (no)=>{
        setMenuNo(no);
    };
    //드로우 메뉴 열기
    const onClickMenuOpen = ()=>{
        setOpen(true);
    };
    //드로우 메뉴 닫기
    const onClickMenuClose = ()=>{
        setOpen(false);
        setMenuNo(0);
    };
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: "mobile-menu",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(material_.AppBar, {
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(material_.Toolbar, {
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx((IconButton_default()), {
                            edge: "start",
                            size: "large",
                            onClick: onClickMenuOpen,
                            children: /*#__PURE__*/ jsx_runtime_.jsx((Menu_default()), {
                                className: "menubar"
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(material_.Typography, {
                            className: "menu-title",
                            children: title
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(material_.Drawer, {
                variant: "persistent",
                open: open,
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "mobile-draw-menu",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "mobile-sign-menu"
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                        onClick: ()=>{
                                            onClickMenu(1);
                                        },
                                        children: [
                                            menuNo === 1 ? /*#__PURE__*/ jsx_runtime_.jsx((KeyboardArrowDown_default()), {}) : /*#__PURE__*/ jsx_runtime_.jsx((KeyboardArrowRight_default()), {}),
                                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                children: "서비스 개요"
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: menuNo === 1 ? "sub-menu-open" : "sub-menu",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/service1",
                                                        children: "서비스 목적"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/service2",
                                                        children: "서비스 절차"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/service3",
                                                        children: "서비스 주요내용"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/service4",
                                                        children: "서비스 운영체제"
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                        onClick: ()=>{
                                            onClickMenu(2);
                                        },
                                        children: [
                                            menuNo === 2 ? /*#__PURE__*/ jsx_runtime_.jsx((KeyboardArrowDown_default()), {}) : /*#__PURE__*/ jsx_runtime_.jsx((KeyboardArrowRight_default()), {}),
                                            "저장행동의 이해",
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: menuNo === 2 ? "sub-menu-open" : "sub-menu",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/understand1",
                                                        children: "저장강박과 자기방임"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/understand2",
                                                        children: "저장강박과 자기방임의 특성"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/understand3",
                                                        children: "저장위기가구의 진단"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/understand4",
                                                        children: "관련 주요 질환"
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                        onClick: ()=>{
                                            onClickMenu(3);
                                        },
                                        children: [
                                            menuNo === 3 ? /*#__PURE__*/ jsx_runtime_.jsx((KeyboardArrowDown_default()), {}) : /*#__PURE__*/ jsx_runtime_.jsx((KeyboardArrowRight_default()), {}),
                                            "사전 서비스 단계",
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: menuNo === 3 ? "sub-menu-open" : "sub-menu",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/advance1",
                                                        children: "서비스 대상 가구 선정"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/advance2",
                                                        children: "서비스 준비 절차"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/advance3",
                                                        children: "사전 교육 및 안전관리"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/advance4",
                                                        children: "사전 준비물 점검"
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                        onClick: ()=>{
                                            onClickMenu(4);
                                        },
                                        children: [
                                            menuNo === 4 ? /*#__PURE__*/ jsx_runtime_.jsx((KeyboardArrowDown_default()), {}) : /*#__PURE__*/ jsx_runtime_.jsx((KeyboardArrowRight_default()), {}),
                                            "서비스 진행 단계",
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: menuNo === 4 ? "sub-menu-open" : "sub-menu",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/progress1",
                                                        children: "서비스 유형별 절차"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/progress2",
                                                        children: "서비스 제공 단계별 실행 가이드"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/progress3",
                                                        children: "폐기물 처리"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/progress4",
                                                        children: "주거청소"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/progress5",
                                                        children: "소독\xb7방역"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/progress6",
                                                        children: "상황 발생에 대한 예방 및 관리대책"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {})
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                        onClick: ()=>{
                                            onClickMenu(5);
                                        },
                                        children: [
                                            menuNo === 5 ? /*#__PURE__*/ jsx_runtime_.jsx((KeyboardArrowDown_default()), {}) : /*#__PURE__*/ jsx_runtime_.jsx((KeyboardArrowRight_default()), {}),
                                            "사후 서비스 단계",
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: menuNo === 5 ? "sub-menu-open" : "sub-menu",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/after1",
                                                        children: "대상가구 교육 및 입주 관리"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/after2",
                                                        children: "서비스 종결 점검"
                                                    }),
                                                    " ",
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/after3",
                                                        children: "서비스 중단 및 재발에 대한 재개입 서비스"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                        href: "/content/after4",
                                                        children: "사후 지원 서비스"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {})
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        })
                    ]
                })
            }),
            /*드로우 메뉴를 열때 배경*/ open ? /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "menu-background",
                onClick: onClickMenuClose
            }) : null
        ]
    });
};
/* harmony default export */ const component_MobileMenuComponent = (MobileMenuComponent);

;// CONCATENATED MODULE: ./component/MenuComponent.js





const MenuComponent = ({ user , title  })=>{
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(component_PcMenuComponent, {}),
            /*#__PURE__*/ jsx_runtime_.jsx(component_MobileMenuComponent, {
                user: user,
                title: title
            })
        ]
    });
};
/* harmony default export */ const component_MenuComponent = (MenuComponent);


/***/ }),

/***/ 1468:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1664);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5692);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_mui_material__WEBPACK_IMPORTED_MODULE_3__);




const UserMenuComponent = ({ user  })=>{
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "user-menu"
        })
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (UserMenuComponent);


/***/ })

};
;