import React from "react";
import Bounce from "react-reveal/Bounce";
import {Container} from "@mui/material";

const Progress6Component = () => {
    return (
        <div>
            <div className='service-content1 progress-background'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            상황 발생에 대한<br/> 예방 및 관리대책
                            <div className='subtitle'>

                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            상황 발생에 대한 예방 및 관리대책
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='progress-content2'>
                <Container>
                    <Bounce top>
                        <ul>
                            <li>
                                <div className="title">가. 서비스 제공 중 발생 가능한 주요 상황</div><br/>
                                1) 이웃 가구로의 해충 등 유해물질 전염으로 인한 민원<br/>
                                2) 폐기물 배출 과정에서 주거 공용 공간의 오염<br/>
                                3) 서비스 제공 중 대상자의 갑작스러운 서비스 거부<br/>
                                4) 대상자의 주요 물품 분실<br/>
                            </li>
                            <li>
                                <div className="title">나. 상황별 예방 및 관리대책</div><br/>
                                1) 방역망 또는 방역가림막을 설치하여 이웃 가구로의 해충 등 유해물질 유입 차단<br/>
                                2) 폐기물 배출 후 주거 공용 공간의 점검 및 정리<br/>
                                3) 서비스 제공 동안에 대상자와 서비스 진행에 대한 정보 공유 및 의견 경청<br/>
                                4) 대상자의 서비스 거부시 철수 후 일정 재협의<br/>
                                5) 서비스 제공 전 대상자의 귀중품 재차 확인<br/>
                            </li>

                        </ul>
                    </Bounce>
                </Container>
            </div>
        </div>
    )
}

export default Progress6Component;