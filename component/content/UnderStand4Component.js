import React from "react";
import Bounce from 'react-reveal/Bounce';
import Fade from 'react-reveal/Fade';
import {Box, Container} from "@mui/material";
import Tab from '@mui/material/Tab';
import {TabContext, TabList, TabPanel} from "@mui/lab";
;

const UnderStand4Component = () => {
    const [value, setValue] = React.useState('1');

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    return (
        <div>
            <div className='service-content1 under1_back'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            저장행동의 이해
                            <div className='subtitle'>
                                관련주요질환
                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            저장위기가구는 정신적 질환인 저장장애와 생활기능 약화에 따른 환경불결 상태를 보이는 자기방임을 모두 포함한다. 이에 두 유형의 저장위기가구를 발굴해내기 위해서는 정서 진단과 행동 진단을 함께 사용할 것을 제안한다.
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='service-content1-2'>
            <Container>
                <Box sx={{ width: '100%', typography: 'body1' }}>
                    <TabContext value={value} allowScrollButtonsMobile>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                            <TabList onChange={handleChange}
                                     scrollButtons
                                     allowScrollButtonsMobile
                                     variant="scrollable"
                            >
                                <Tab label="강박장애" value="1" style={{fontSize:'15px',fontWeight:'bold'}}/>
                                <Tab label="불안장애" value="2"  style={{fontSize:'15px',fontWeight:'bold'}}/>
                                <Tab label="충동조절장애" value="3"  style={{fontSize:'15px',fontWeight:'bold'}}/>
                                <Tab label="우울장애" value="4"  style={{fontSize:'15px',fontWeight:'bold'}}/>
                                <Tab label="물질관련 및 중독성 장애" value="5"  style={{fontSize:'15px',fontWeight:'bold'}}/>
                            </TabList>
                        </Box>
                        <TabPanel value="1" style={{fontSize:'18px'}}>

                            1) <b>강박장애(obsessive-compulsive disorder)란</b><br/>
                            ∙ 어떤 반복되는 사고와 행동으로부터 자신이 원치 않음에도 불구하고 벗어나지 못한 채 계속해서 의무적으로 시달리게 되는 증상을 의미<br/>
                            ∙ 강박적 사고와 강박행동이 동반되어 나타남<br/>
                            <br/><br/>
                            2) <b>원인</b><br/>
                            ∙ 강박사고나 이로 인한 불안, 괴로움을 예방하거나 감소 등을 위한 심리학적 요인<br/>
                            ∙ 특정 신경회로 영역 결함 등의 생물학적 요인<br/><br/>

                            3) <b>주요 증상</b><br/>
                            ∙ 오염 강박사고에 따른 반복적 손 씻기 및 정리행동<br/>
                            ∙ 대칭성에 대한 강박사고에 따른 정리정돈 및 숫자 세기<br/>
                            ∙ 공격적, 성적, 종교적 강박사고와 관련 강박행동<br/>
                            ∙ 자해나 타해에 대한 공포에 따른 반복적 확인행동<br/>

                        </TabPanel>
                        <TabPanel value="2" style={{fontSize:'18px'}}>
                            1) <b>불안장애(anxiety disorder)란</b><br/>
                            ∙ 병적인 불안과 공포로 인해 과도한 심리적 고통을 느끼거나 현실적인 적응에 심각한 어려움을 겪는 정신장애를 의미<br/><br/>

                            2) <b>원인</b><br/>
                            ∙ 불안이나 우울과 같은 정서적인 부분을 담당하는 뇌 신경회로 내의 신경전달물질의 부족 또는 과다 및 유전적 소인으로 인한 생물학적 요인<br/>
                            ∙ 다양한 사회적 상황에서 창피를 당하거나 난처해지는 것에 대한 과도한 두려움으로 인한 사회심리학적인 요인<br/>
                            ∙ 과거의 경험과 현재의 받아들인 정보를 해석하고 판단하는 인지적 측면의 결함 요인<br/><br/>

                            3) <b>주요 증상</b><br/>
                            ∙ 자율신경계 반응에 의해 나타나는 호흡과 맥박의 증가, 근육 긴장 등의 신체적 불안<br/>
                            ∙ 행동이나 생각으로 나타나는 정신적 불안(공황장애, 광장공포증, 범불안장애, 사회불안장애, 특정 공포증, 분리불안 장애, 선택적 함구증 등)
                        </TabPanel>
                        <TabPanel value="3" style={{fontSize:'18px'}}>
                            1) <b>충동조절장애(impulse control disorders)란</b><br/>
                            ∙ 본능적 욕구가 지나치게 강하거나 자기방어 기능이 약해져서 자신이나 타인을 해하려는 충동이나 유혹을 조절하지 못하는 정신장애를 의미<br/>
                            ∙ 자신의 행동에 대해 스스로 이상하다고 느끼지 않음에 따라 대개 충동적 행위 후 자책, 후회, 죄책감과 같은 감정을 느끼지 않음<br/>
                            ∙ 강박증 및 중독과 유사한 양상을 보임<br/><br/>

                            2) <b>원인</b><br/>
                            ∙ 유전적 요인과 뇌의 물리적 손상 등에 의한 생물학적 요인<br/>
                            ∙ 부적절한 가정환경 및 교육 등에 의한 환경적 요인<br/><br/>

                            3) <b>주요 증상</b><br/>
                            ∙ 심각한 폭력이나 파괴적 행동과 같은 간헐성 폭발장애<br/>
                            ∙ 병적 도벽/방화/도박<br/>
                            ∙ 물질 사용 장애(알코올이나 약물 등에 대한 의존 및 남용)<br/>
                            ∙ 기타 충동조절 장애(자해, 인터넷 중독, 쇼핑중독, 손톱 물어뜯기 등)
                        </TabPanel>
                        <TabPanel value="4" style={{fontSize:'18px'}}>
                            1) <b>우울장애(depressive disorder)란</b><br/>
                            ∙ 우울한 기분에 대해 주관적으로 보고하거나 타인에 의해 객관적으로 관찰되고,<br/>
                            &nbsp;&nbsp;&nbsp;생각의 내용, 사고과정, 동기, 의욕, 관심, 행동, 수면, 신체활동 등<br/>
                            &nbsp;&nbsp;&nbsp;전반적인 정신기능의 저하가 긴 시간 지속되는 상태를 의미<br/><br/>

                            2) <b>원인</b><br/>
                            ∙ 유전적 요인, 신경전달물질의 불균형, 내분비 이상 등의 생물학적 요인<br/>
                            ∙ 스트레스, 성격특성, 대인관계 문제, 아동기 갈등 등 심리적 요인<br/>
                            ∙ 생물학적 요인과 심리적 요인이 복합적으로 작용하여 발병<br/><br/>

                            3) <b>주요 증상</b><br/>
                            ∙ 우울한 기분<br/>
                            ∙ 즐거움 상실<br/>
                            ∙ 체중의 증가 또는 감소<br/>
                            ∙ 불면 또는 과다수면<br/>
                            ∙ 피로감 및 활력 상실<br/>
                            ∙ 낮은 자존감 및 죄책감<br/>
                            ∙ 정신운동성 지체<br/>
                            ∙ 집중력 감소 및 우유부단함
                        </TabPanel>
                        <TabPanel value="5" style={{fontSize:'18px'}}>
                            1) <b>물질관련 및 중독성 장애(substance related and addictive disorder)란</b><br/>
                            ∙ 중독성을 지닌 물질이나 약물의 반복적인 사용에 따른 신체적, 심리적 장애<br/>
                            ∙ 사회적 또는 직업상의 기능장애를 초래하는 물질을 정상 범위를 넘어서 지속적 혹은 주기적으로 사용하여,<br/>
                              &nbsp;&nbsp;&nbsp;정신적·신체적 변화를 일으켜 해로운 결과가 있음에도 불구하고<br/>
                            &nbsp;&nbsp;&nbsp;스스로 물질 사용을 중단하거나 조절하지 못하고 강박적으로 사용하는 상태<br/>
                            ∙ 물질에는 합법적 물질과 비합법적 물질이 있음<br/>
                            - 합법적 물질: 약물, 알코올, 카페인, 담배 등<br/>
                            - 비합법적 물질: 정신자극제, 환각제, 방향성 물질 등<br/><br/>

                            2) <b>원인</b><br/>
                            ∙ 관련 다른 정신장애를 앓고 있는 이중 진단, 만성 통증, 충동성 등의 개인적 요인<br/>
                            ∙ 조건이 충족되었을 때 쾌락과 같은 보상으로 인해 이후에도 반복적으로 그 행동을 하도록 유도하는 뇌 보상회로의 작용 등과<br/>
                            &nbsp;&nbsp;&nbsp;같은 생물학적 요인<br/>
                            ∙ 사회적 소외감, 부정적 감정의 완화 등을 위한 심리학적 요인<br/>
                            ∙ 물질의 남용, 특히 술과 담배 등에 대한 사회문화적 관습과 태도, 물질의 제공(입수 가능성), 같은 남용자들의 유혹과 압력 등의<br/>
                            &nbsp;&nbsp;&nbsp;사회문화적 요인<br/>
                            ∙ 약물 효과가 점차 감소하여 용량을 증가시켜야 같은 효과를 얻는 내성(tolerance)의 발생과 같은 신체적 요인<br/><br/>

                            3) <b>주요 증상</b><br/>
                            ∙ 물질의 획득과 사용에 많은 시간을 할애<br/>
                            ∙ 중추신경계에 영향을 주어 사고력, 기억력, 집중력, 판단력 등의 장애<br/>
                            ∙ 물질을 중단하거나 다른 물질로 중화시 불쾌한 신체적 효과인 금단 증상 발생<br/>
                            ∙ 환각증, 조현병 등과 같은 정신장애

                        </TabPanel>
                    </TabContext>
                </Box>
            </Container>
            </div>
        </div>
    )
}

export default UnderStand4Component;