import React from "react";
import Bounce from "react-reveal/Bounce";
import {Container} from "@mui/material";

const After2Component = () => {
    return (
        <div>
            <div className='service-content1 after-background'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            서비스 종결 점검
                            <div className='subtitle'>

                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            서비스 종결 점검
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='progress-content2'>
                <Container>
                    <Bounce top>
                        <ul>
                            <li>
                                <div className="title">가. 종결 평가</div><br/>
                                1) 서비스 평가<br/>
                                ∙ 서비스 제공 계획과 실제 제공된 서비스의 일치 정도 확인<br/>
                                ∙ 서비스 제공 여건 변화 여부 확인<br/>
                                ∙ 서비스 활동의 가치 확인<br/>
                                ∙ 업체별 계약에 따라 서비스 목적이 달성되었는지 확인<br/>
                                ∙ 서비스 제공업체의 평가<br/>
                                ∙ 사례관리자에 의한 평가<br/>
                                ∙ 이미지 척도(Clutter Image Rating Scale)를 사용하여 정리정돈의 정도를 재측정<br/><br/>

                                2) 대상자 평가<br/>
                                ∙ 이미지 척도를 사용하여 주거환경개선 정도를 평가<br/>
                                ∙ 저장강박 또는 자기방임의 정도를 재측정하여 서비스 제공 전후의 증상 비교<br/>
                                ∙ 대상자 서비스 만족도(서비스 양, 내용, 품질 등) 평가<br/>
                            </li>
                            <li>
                                <div className="title">나. 종결 점검</div><br/>
                                1) 현장 점검<br/>
                                ∙ 대상자의 주거 청결 상태 확인<br/>
                                ∙ 사례관리자의 서비스 상태 최종 확인 및 종결<br/><br/>

                                2) 대상자 점검<br/>
                                ∙ 서비스 제공을 통한 대상자 변화 정도(수립된 성과목표 일치성, 대상자의 삶의 변화) 확인<br/>
                                ∙ 대상자의 긍정적 변화 상태를 유지하기 위한 예방계획 수립<br/>
                                ∙ 종결에 대한 사전 안내를 통해 준비 시간을 갖도록 유도<br/>
                                ∙ 대상자의 향후 주거 청결 관리에 대한 의지 확인<br/>
                            </li>
                        </ul>
                    </Bounce>
                </Container>
            </div>
        </div>
    )
}

export default After2Component;