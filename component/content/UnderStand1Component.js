import React from "react";
import Bounce from 'react-reveal/Bounce';
import {Container} from "@mui/material";

const UnderStand1Component = () => {
    return (
        <div>
            <div className='service-content1 under1_back'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            저장행동의 이해
                            <div className='subtitle'>
                                저장강박과 자기방임
                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            저장위기가구는 사회적 위생기준을 유지하지 못하며 의도적
                            또는 비의도적으로 심각한 저장행동을 보이는 가구로,<br/>
                            이는 정신질환인 저장강박으로 인해 저장행동을 보이는
                            대상자와 저장강박의 정도와 관계없이 행동적 문제인
                            자기방임으로 인해 저장행동을 보이는 대상자를 모두 포함합니다.<br/>
                            본 매뉴얼에서는 이들을 ‘저장위기가구’로 칭하여 기술합니다.<br/>
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <Bounce top>
                        <div className='title'>
                            저장강박(compulsive hoarding)
                        </div>
                        <div className='subtitle'>
                            저장강박에 대해 알아봅시다.
                        </div>
                    </Bounce>
                    <div className="content">
                        <div className='img'>
                            <Bounce left>
                                <img src="/images/under1_1.jpg"/>
                            </Bounce>
                        </div>
                        <Bounce
                            right
                            duration={2000}
                        >
                            <div className='content-info'>
                                ∙ 물건의 가치에 관계없이 사용하지 않는<br/>
                                  &nbsp;&nbsp;물건을 버리는 것에 어려움을 느껴 과도한 양의 물건을 획득 및 보유하는 상태<br/>
                                ∙ 강박스펙트럼 상 강박성향(좌측)과 충동성향(우측)이 혼재하는 중간 지점에 위치<br/>
                                ∙ 물건에 대한 감정적 애착과 물건을 상실하는 것에 대한 두려움이 강화역할을 하여<br/>
                                &nbsp;&nbsp;저장강박행동의 지속에 기여<br/>
                                ∙ 보유행동과 과도한 획득행동이 동반되어 발현<br/>
                                ∙ 청결과 위생의 사회적 기준에서 이탈된 상태로, 심각한 저장강박 증상과<br/>
                                &nbsp;&nbsp;과도한 저장행동을 모두 보유<br/>
                                ∙ 2013년 이전까지 ‘저장강박장애’로 불리어졌으나, 2013년에 개정된 DSM-5에서 부터<br/>
                                &nbsp;&nbsp;‘저장장애’라는 공식적인 진단분류 체계에 수록되었으며, 강박 및 관련장애 범주의 <br/>
                                &nbsp;&nbsp;하위유형에 해당<br/>
                                ∙ 저장장애의 유병율은 전체 인구의 약 2~5%로 추정(Pertusa 외 2008)<br/>
                                &nbsp;&nbsp;Pertusa, A., M. A. Fullana, S. Singh, P. Alonso, J. M. Menchon,<br/>
                                &nbsp;&nbsp;and D. Mataix-Cols(2008), "Compulsive Hoarding: OCD Symptom,<br/>
                                &nbsp;&nbsp;Distinct Clinical Syndrome, or Both?" The America Journal of Psychiatry,<br/>
                                &nbsp;&nbsp;165(10), 1289-1298.

                            </div>
                        </Bounce>
                    </div>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <Bounce top>
                        <div className='title'>
                            자기방임(self-neglect)
                        </div>
                        <div className='subtitle'>
                            자기방임에 알아봅시다.
                        </div>
                    </Bounce>
                    <div className="content">

                        <Bounce
                            left
                            duration={2000}
                        >
                            <div className='content-info'>
                                ∙ 주어진 문화에서 사회적으로 수용된 개인위생과 건강상태를 유지하는데<br/>
                                  &nbsp;&nbsp;필요한 활동들을 수행하지 못하는 것으로, 의식주 제공 및 의료처치 등<br/>
                                &nbsp;&nbsp;최소한의 자기보호 관련 행동을 의도적으로 포기하거나 비의도적으로<br/>
                                &nbsp;&nbsp;관리하지 않는 상태를 지칭<br/>
                                ∙ 청결과 위생의 사회적 기준에서 이탈된 상태로, 과도한 저장행동을 보임<br/>
                                ∙ 열악한 개인위생, 쓰레기 수집, 불결한 환경, 원조에 대한 거부, 사회적 철회, 수치심을<br/>
                                &nbsp;&nbsp;못 느끼는 태도 등을 보이며, 최근 들어 정신질환이나 인지적 손상과 관계없이<br/>
                                &nbsp;&nbsp;관찰되고 있음<br/>
                                ∙ 국내의 경우, 자기결정에 의한 의도적 자기방임과<br/>
                                &nbsp;&nbsp;정신장애(우울, 치매, 알코올중독, 지적장애 등)로 인한 비의도적 자기방임으로 구분<br/>
                                ∙ 해외의 경우, 자기방임을 결정함에 있어 정신적, 신체적 손상 및 무능력을<br/>
                                &nbsp;&nbsp;주요 전제로 고려함과 아울러 의학적 진단과정을 중요시<br/>
                                ∙ 디오게네스 신드롬(diogenes syndrome), 노인성손상 신드롬(senile breakdown syndrome),
                                가정불결(domestic squalor)이란 용어로 사용되고 있음
                            </div>
                        </Bounce>
                        <div className='img'>
                            <Bounce right>
                                <img src="/images/under1_2.jpg"/>
                            </Bounce>
                        </div>
                    </div>

                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <Bounce top>
                        <div className='title'>
                            저장위기가구의 이해
                        </div>
                    </Bounce>
                    <div className="content">

                        <Bounce
                            bottom
                            duration={2000}
                        >
                            <div className='content-info'>
                                사회적 위생기준을 유지하지 못하며 의도적 또는 비의도적 저장행동을 보이는 가구로, 저장강박으로 인한 저장행동 대상자와 자기방임으로 인한 저장행동 대상자를 모두 포함하여 칭함
                            </div>
                        </Bounce>

                    </div>

                </Container>
            </div>
        </div>
    )
}

export default UnderStand1Component;