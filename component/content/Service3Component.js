import React from "react";
import Bounce from 'react-reveal/Bounce';
import {Container} from "@mui/material";

const Service3Component = () => {
    return (
        <div>
            <div className='service-content1 service3-background'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            사전 서비스
                            <div className='subtitle'>
                                서비스를 사전에 준비해 보세요.
                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            1) 서비스 대상 가구 선정<br/><br/><br/>
                            2) 서비스 준비 절차<br/><br/><br/>
                            3) 사전 교육 및 안전관리<br/><br/><br/>
                            4) 사전 준비물 점검
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <Bounce top>
                        <div className='title'>
                            서비스 진행
                        </div>
                        <div className='subtitle'>
                            서비스는 확실하게 진행하겠습니다.
                        </div>
                    </Bounce>
                    <div className="content">
                        <div className='img'>
                            <Bounce left>
                                <img src="/images/service_3_1.jpg"/>
                            </Bounce>
                        </div>
                        <Bounce
                            right
                            duration={2000}
                        >
                            <div className='content-info'>
                                1) 폐기물 처리 서비스<br/><br/>
                                2) 주거청소 서비스<br/><br/>
                                3) 소독·방역 서비스<br/><br/>
                                4) 상황 발생에 대한 예방 및 관리
                            </div>
                        </Bounce>
                    </div>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <Bounce top>
                        <div className='title'>
                            사후 서비스
                        </div>
                        <div className='subtitle'>
                            서비스 진행 후 꾸준히 관리하겠습니다.
                        </div>
                    </Bounce>
                    <div className="content">

                        <Bounce
                            top
                            duration={2000}
                        >
                            <div className='content-info'>
                                1) <b>대상가구 교육 및 입주 관리</b><br/>
                                ∙ 대상자 교육<br/>
                                ∙ 대상자 입주 관리<br/><br/>
                                2) <b>서비스 종결 점검</b><br/>
                                ∙ 종결 평가<br/>
                                ∙ 종결 점검<br/><br/>
                                3) <b>서비스 중단 및 재발에 대한 재개입 서비스</b><br/>
                                ∙ 중단가구에 대한 직·간접 (재)개입 서비스<br/>
                                ∙ 재발가구에 대한 직·간접 (재)개입 서비스<br/><br/>
                                4) <b>사후 지원 서비스</b><br/>
                                ∙ 사후 모니터링<br/>
                                ∙ 상담 및 심리치료<br/>
                                ∙ 사회연계 프로그램 지원
                            </div>
                        </Bounce>
                        <div className='img'>
                            <Bounce bottom>
                                <img src="/images/service_3_2.jpg"/>
                            </Bounce>
                        </div>
                    </div>

                </Container>
            </div>
        </div>
    )
}

export default Service3Component;