import React, {useState} from "react";
import {Container} from "@mui/material";
import Bounce from "react-reveal/Bounce";
import Fade from "react-reveal/Fade";
import TabLayout from "@/component/TabLayout";

const Adance1Component = () => {
    const tabMenu = [
        {id : 1,menu:'사후 모니터링'},
        {id : 2,menu:'상담 및 심리치료'},
        {id : 3,menu:'사회연계 프로그램 지원'},
    ];
    const [index_no,setIndexNo] = useState(1);
    const onClickTab = (no) => {
        setIndexNo(no);
    }
    return (
        <div>
            <div className='service-content1 after-background'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            사후 지원 서비스
                            <div className='subtitle'>

                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            사후 지원 서비스

                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <TabLayout
                        index_no={index_no}
                        tabMenu={tabMenu}
                        onClickTab={onClickTab}
                    />
                    <div className='content'>
                        {
                            index_no===1?
                                <div className='content-info2'>
                                    <Fade>
                                        1) 서비스 종결 후, 일정 기간을 설정하여 대상자 가구의 변화 및 유지 여부, 재개입의 필요성 등을 판단하기 위해 실시<br/><br/>

                                        2) 사후 모니터링 결과, 새로운 문제나 욕구가 발생한 경우 재개입 필요성 등을 판단하여 위기상황의 재발 예방<br/><br/>

                                        3) 대상자의 서비스 만족도 관리 및 추가 서비스 확인<br/>
                                        ∙ 서비스 종결 이후 주거환경개선 서비스에 대한 만족도 관리<br/>
                                        ∙ 서비스 결함 및 미충족된 욕구 확인<br/>
                                        ∙ 추가 지원서비스 확인<br/><br/>

                                        4) 대상자 주거 청결 관리에 대한 정기 모니터링<br/>
                                        ∙ 대상자 욕구와 상황의 변화 파악<br/>
                                        ∙ 대상자 재발 여부의 조기 판단<br/>
                                        ∙ 대상자의 새로운 욕구에 대응<br/>
                                        ∙ 투입된 자원과 지원한 내부 자원이 잘 기능하는지 등의 확인<br/>
                                        ∙ 재발가능성이 높은 종결 대상자의 경우, 사후 모니터링과 함께 사후관리 계획 수립 필요<br/>
                                        ∙ 전문적 접근이 필요한 경우, 지역내 전문기관에 이관<br/>
                                        ∙ 서비스 제공 후 평균 6개월의 기간 동안 실시<br/><br/>

                                        5) 사후 모니터링에 따른 조치<br/>
                                        ∙ 욕구 재조사 및 서비스 제공 계획 재수립<br/>
                                        - 대상자 가구를 둘러싼 환경 변화 발생으로 재검토가 필요한 경우<br/>
                                        - 심각한 신규 욕구 또는 문제가 발생한 경우<br/>
                                        ∙ 대상자 가구의 긍정적 변화에 의한 종결<br/>
                                        - 대상자 가구의 상황 호전<br/><br/>

                                        6) 사후 모니터링 방법들<br/>
                                        ∙ 가정방문<br/>
                                        ∙ 전화통화<br/>
                                        ∙ 설문조사<br/>
                                        ∙ 관찰<br/>
                                    </Fade>
                                </div>:null
                        }
                        {
                            index_no===2?
                                <div className='content-info2'>
                                    <Fade>
                                        1) 담당 사례관리자의 상담 진행<br/>
                                        ∙ 사후 모니터링과 병행<br/><br/>

                                        2) 기관 연계를 통한 상담 및 심리치료 지원<br/>
                                        ∙ 정신건강복지센터<br/>
                                        ∙ 사회복지기관<br/>
                                        ∙ 구청 희망복지지원단<br/>
                                        ∙ 동주민센터<br/>
                                        ∙ 심리상담센터<br/>
                                        ∙ 의료기관<br/>
                                        ∙ 기타 민간기관<br/>
                                    </Fade>
                                </div>:null
                        }
                        {
                            index_no===3?
                                <div className='content-info2'>
                                    <Fade>
                                        1) 지역사회 연계활동<br/>
                                        ∙ 지역주민들을 지원하는 캠페인, 지원, 봉사활동을 수행<br/>
                                        ∙ 지역주민을 지원하는 활동을 통해 대상자의 저장강박 및 자기방임 등의 부정적 증상 완화 기대<br/><br/>

                                        2) 지역사회 문화체험<br/>
                                        ∙ 지역주민과 함께 지역사회 문화체험 활동을 통해 지역사회 구성원으로서 소속감과 사회성 향상<br/><br/>

                                        3) 사회적응 프로그램<br/>
                                        ∙ 생활지원 프로그램을 통해 개인 생활을 유지하는데 필요한 문제해결능력의 습득 및 사회성 향상<br/>
                                        ∙ 후원, 봉사 커뮤니티 참여
                                    </Fade>
                                </div>:null
                        }

                    </div>
                </Container>
            </div>

        </div>
    )
}

export default Adance1Component;