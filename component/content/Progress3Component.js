import React, {useState} from "react";
import Bounce from "react-reveal/Bounce";
import {Container} from "@mui/material";
import Fade from "react-reveal/Fade";
import TabLayout from "@/component/TabLayout";

const Progress3Component = () => {
    const tabMenu = [
        {id : 1,menu:'작업준비물'},
        {id : 2,menu:'폐기물 처리 서비스 절차 및 내용'},
        {id : 3,menu:'작업시 유의사항'}
    ];

    const [index_no,setIndexNo] = useState(1);
    const onClickTab = (no) => {
        setIndexNo(no);
    }
    return (
        <div>
            <div className='service-content1 progress-background'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            폐기물 처리
                            <div className='subtitle'>

                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            폐기물 처리
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <TabLayout
                        index_no={index_no}
                        tabMenu={tabMenu}
                        onClickTab={onClickTab}
                    />
                    <div className='content'>
                        {
                            index_no===1?
                                <div className='content-info2'>
                                    <Bounce top>
                                        <div className="title">가. 작업 준비물</div><br/>
                                        1) 작업 준비물 리스트<br/>
                                        ∙ 방진복: 오염된 외부 환경으로부터 작업자의 안전 도모<br/>
                                        ∙ 방진마스크: 작업시 유해물질(미세먼지 등) 흡입 예방<br/>
                                        ∙ 안전모, 안전화: 신체보호<br/>
                                        ∙ 위생덧신: 개인 방역<br/>
                                        ∙ 코팅장갑, 고무장갑: 손보호 및 미끄러짐 방지<br/>
                                        ∙ 헤드램프: 서비스 대상 가구 단전시 준비<br/>
                                        ∙ 마대자루: 폐기물 수거용. 현장 상황에 맞추어 적정 용량으로 준비<br/>
                                        ∙ 포장용 끈: 폐기물 처리시 사용<br/>
                                        ∙ 이동용 카트: 폐기물 운반시 사용<br/>
                                        ∙ 사다리차: 대상 가구가 고층에 위치한 경우 사용. 폐기물 운반 과정의 이웃민원 방지<br/>
                                        ∙ 다용도 봉투: 재활용품 수거 용도<br/>
                                        ∙ 방역망, 방역가림막: 이웃 가구로의 해충 등 유해물질 유입 차단<br/><br/>
                                    </Bounce>
                                    <Fade>
                                        <div className="mobile-image">
                                            <img src="/images/advance3_10.jpeg"/><br/>
                                            <b>방진복</b><br/><br/>
                                            <img src="/images/advance3_3.jpeg"/><br/>
                                            <b>방진마스크</b><br/><br/>
                                            <img src="/images/cart.jpg"/><br/>
                                            <b>이동용 카트</b><br/><br/>
                                            <img src="/images/rope.jpg"/><br/>
                                            <b>포장용 끈</b><br/><br/>
                                            <img src="/images/gunny.jpg"/><br/>
                                            <b>마대자루</b><br/><br/>
                                            <img src="/images/advance3_5.jpeg"/><br/>
                                            <b>헤드램프</b><br/><br/>
                                            <img src="/images/ladder.jpeg"/><br/>
                                            <b>사다리차</b><br/><br/>
                                            <img src="/images/rubber.jpg"/><br/>
                                            <b>고무장갑</b><br/><br/>
                                            <img src="/images/plastic.jpg"/><br/>
                                            <b>다용도봉투</b><br/><br/>
                                            <img src="/images/screen.jpg"/><br/>
                                            <b>방역가림막</b><br/><br/>
                                            <img src="/images/advance3_4.jpeg"/><br/>
                                            <b>위생덧신</b><br/><br/>
                                            <img src="/images/advance3_1.jpeg"/><br/>
                                            <b>안전모</b><br/><br/>
                                            <img src="/images/advance3_8.jpeg"/><br/>
                                            <b>안전화</b><br/><br/>

                                        </div>
                                        <table className="image-table">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <img src="/images/advance3_10.jpeg"/><br/>
                                                    <b>방진복</b>
                                                </td>
                                                <td>
                                                    <img src="/images/advance3_3.jpeg"/><br/>
                                                    <b>방진마스크</b>
                                                </td>
                                                <td>
                                                    <img src="/images/cart.jpg"/><br/>
                                                    <b>이동용 카트</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="/images/rope.jpg"/><br/>
                                                    <b>포장용 끈</b>
                                                </td>
                                                <td>
                                                    <img src="/images/gunny.jpg"/><br/>
                                                    <b>마대자루</b>
                                                </td>
                                                <td>
                                                    <img src="/images/advance3_5.jpeg"/><br/>
                                                    <b>헤드램프</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="/images/ladder.jpeg"/><br/>
                                                    <b>사다리차</b>
                                                </td>
                                                <td>
                                                    <img src="/images/rubber.jpg"/><br/>
                                                    <b>고무장갑</b>
                                                </td>
                                                <td>
                                                    <img src="/images/plastic.jpg"/><br/>
                                                    <b>다용도봉투</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="/images/screen.jpg"/><br/>
                                                    <b>방역가림막</b>
                                                </td>
                                                <td>
                                                    <img src="/images/advance3_4.jpeg"/><br/>
                                                    <b>위생덧신</b>
                                                </td>
                                                <td>
                                                    <img src="/images/advance3_1.jpeg"/><br/>
                                                    <b>안전모</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colSpan={3}>
                                                    <img src="/images/advance3_8.jpeg"/><br/>
                                                    <b>안전화</b>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </Fade>
                                </div>:null
                        }
                        {
                            index_no===2?
                                <div className='content-info2'>
                                    <Fade>
                                        1) 폐기물 확인<br/>
                                        ∙ 대상자 및 사례관리자(또는 주민센터 담당자)와 협의된 폐기물 확인<br/>
                                        ∙ 대상자와 사례관리자의 동의를 얻은 후 현장 사진 촬영<br/><br/>

                                        2) 작업 준비 및 인원 구성<br/>
                                        ∙ 작업공간 확보<br/>
                                        - 작업 전 공간확보를 통해 안전관리<br/>
                                        ∙ 인원 구성 및 배치<br/>
                                        - 폐기물 용량에 따라 인원 구성 <br/>
                                        - 인력 배치<br/><br/>

                                        3) 폐기물 선별<br/>
                                        ∙ 폐기물 선별 및 폐기물 최종 확인<br/>
                                        - 사례관리자와 폐기물 선별에 대한 협의 진행<br/>
                                        - 대상자와 재차 선별작업 진행<br/>
                                        - 중요 물품(귀금속, 앨범, 통장, 기타 법률서류 등)의 경우 사례관리자와 대상자에게 안내 후 구분 보관<br/>
                                        - 폐기물로 분류된 물건이더라도 복원할 수 있거나 재사용이 가능한 물건 확인<br/>
                                        - 폐기물로 분류되어 있지 않은 물건 처리 재확인<br/>
                                        - 사전답사시 확인되지 않은 물건의 처리 확인<br/>
                                        - 위의 작업 결과, 폐기물량이 늘어났을 경우 사례관리자(또는 주민센터 담당자)와 해결 방안 협의<br/>
                                        * 소량(o.5톤 미만)일 경우는 서비스 무료 제공<br/>
                                        * 대량(0.5톤 이상)일 경우는 관계자에 전달하여 계약변경을 위한 예산 추가 가능 여부 확인<br/><br/>

                                        4) 폐기물 분류 및 포장<br/>
                                        ∙ 사례관리자와 폐기물 분류에 대한 협의 진행<br/>
                                        ∙ 현장 책임자는 폐기물과 일반 물품을 명확히 구분하여 작업자에게 명시<br/>
                                        ∙ 선별하여 마대자루(폐기물), 지자체용 종량제 봉투, 재활용 투명봉투에 담아 분류<br/>
                                        - 재활용품에 있어 관리실이 있는 주거의 경우는 관리실 협조를 통해 지정된 곳에 배출하고, 일반주택의 경우는 지정된 날짜나 관련 지자체에 협조 요청하여 서비스 당일 수거토록 함<br/>
                                        - 오염된 플라스틱 등은 폐기물로 처리<br/><br/>

                                        5) 폐기물 이동<br/>
                                        ∙ 폐기물 외부 반출<br/>
                                        - 아파트의 경우 엘리베이터 사용가능 여부 파악<br/>
                                        ∙ 필요시 사다리차를 이용하여 반출<br/>
                                        - 중규모 이상의 폐기물, 입구가 막혀있는 주택이나 고층 아파트의 경우<br/>
                                        - 사다리차 이용시 안전요원 배치 및 폐기물 낙하 예방을 위한 그물망 설치 필요<br/>
                                        ∙ 폐기물량에 따라 자체 처리 또는 폐기물 처리업체로 운반<br/><br/>

                                        6) 작업 마무리<br/>
                                        ∙ 미처리 폐기물 확인<br/>
                                        ∙ 사례관리자(또는 주민센터 담당자)에게 작업 보고<br/>
                                        ∙ 서비스 완료 사진 촬영<br/>
                                        ∙ 보고서 제출 및 세금계산서 발행<br/>
                                    </Fade>
                                </div>:null
                        }
                        {
                            index_no===3?
                                <div className='content-info2'>
                                    <Fade>
                                        1) 배출 관련 추가 협의 과정에서 대상자와의 의견충돌로 서비스 진행이 불가하다고 판단되면 바로 철수한 뒤 추후 일정 재협의<br/>
                                        2) 폐기물 배출시 가능한 한 대상자와 함께 진행하며 처리 중 돌발상황을 대비하여 사례관리자(또는 주민센터 담당자)가 대상자 근처에 있도록 함<br/>
                                        3) 거동이 불편한 대상자는 서비스 제공 기간 동안 병원이나 가족(친지)의 집에서 머물도록 권유<br/>
                                        4) 폐기물 처리시 대상자와 함께 진행해야 할 경우에는 대상자의 의견을 존중하며 진행<br/>
                                        5) 폐기물 운반시 벽면이나 바닥 등이 오염되지 않도록 유의<br/>
                                        6) 분류된 폐기물 구분 배출<br/>
                                        7) 폐기물 운반 차량의 공간 활용과 안전사고 예방을 위해 폐가구부터 운반하여 차량의 가장자리에 적재하고 남는 공간에 폐기물 적재<br/>
                                        8) 사다리차 사용시 낙하물 안전사고 유의<br/>
                                        9) 현장 책임자(또는 작업자)와 대상자의 성별을 최대한 맞추도록 함<br/>
                                    </Fade>
                                </div>:null
                        }
                    </div>
                </Container>
            </div>
        </div>
    )
}

export default Progress3Component;