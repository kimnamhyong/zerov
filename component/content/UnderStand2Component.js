import React from "react";
import Bounce from 'react-reveal/Bounce';
import {Container} from "@mui/material";

const UnderStand2Component = () => {
    return (
        <div>
            <div className='service-content1 under1_back'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            저장행동의 이해
                            <div className='subtitle'>
                                저장강박과 자기방임의 특성
                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            저장위기가구는 사회적 위생기준을 유지하지 못하며 의도적
                            또는 비의도적으로 심각한 저장행동을 보이는 가구로,<br/>
                            이는 정신질환인 저장강박으로 인해 저장행동을 보이는
                            대상자와 저장강박의 정도와 관계없이 행동적 문제인
                            자기방임으로 인해 저장행동을 보이는 대상자를 모두 포함합니다.<br/>
                            본 매뉴얼에서는 이들을 ‘저장위기가구’로 칭하여 기술합니다.<br/>
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <Bounce top>
                        <div className='title'>
                            저장강박의 특성
                        </div>
                        <div className='subtitle'>
                            저장강박 특성에 대해 알아봅시다.
                        </div>
                    </Bounce>
                    <div className="content">
                        <div className='img'>
                            <Bounce left>
                                <img src="/images/under2_1.jpg"/>
                            </Bounce>
                        </div>
                        <Bounce
                            right
                            duration={2000}
                        >
                            <div className='content-info'>
                                <b>사고, 신념, 태도적 특성</b><br/>
                                • 소유물에 대한 감정적 애착<br/>
                                • 소유물과 관련된 기억상실에 대한 염려<br/>
                                • 소유물에 대한 통제력 상실<br/>
                                • 소유물에 대한 과도한 책임 의무<br/><br/>
                                <b>정보처리 관련 특성</b><br/>
                                • 기능적 손상으로 인한 의사결정의 어려움<br/>
                                • 조직화 및 범주화의 어려움<br/>
                                • 기억 손상<br/><br/>
                                <b>행동적 특성</b><br/>
                                • 소유물 처분 회피<br/>
                                • 소유물에 대한 의사결정의 회피<br/>
                                • 가치가 없는 물건의 지속적 수집<br/><br/>
                                <b>개인적 및 사회적 특성</b><br/>
                                • 낮은 혼인율<br/>
                                • 인격장애와의 높은 연관성<br/>
                                • 행동에 대한 낮은 통찰력<br/>
                                • 변화에 대한 낮은 동기부여
                            </div>
                        </Bounce>
                    </div>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <Bounce top>
                        <div className='title'>
                            자기방임의 특성
                        </div>
                        <div className='subtitle'>
                            자기방임 특성에 대해 알아봅시다.
                        </div>
                    </Bounce>
                    <div className="content">

                        <Bounce
                            left
                            duration={2000}
                        >
                            <div className='content-info'>
                                ∙ 자기 돌봄 상실, 개인위생관리의 어려움, 영양 및 건강관리 부족 등 자기보호의 부재<br/>
                                ∙ 쓰레기나 잡동사니를 지속적으로 축적하는 심각한 수준의 환경불결 상태를 보임<br/>
                                * 환경불결은 주로 저장강박증에서 비롯되나, 반드시 저장강박 증상이 전제되는 것은 아님<br/>
                                ∙ 사회적 고립과 외부 도움에 대한 강한 저항감 및 수치심의 결여된 상태를 보임<br/>
                                ∙ 지역사회에서 고립된 노인들에게서 빈번하게 관찰되며, 노년기에 발생하는 일상생활<br/>
                                &nbsp;&nbsp;기능과 인지기능의 저하, 노쇠, 정신질환 등이 자기방임의 발생을 높이는 위험요인임<br/>
                                ∙ 자신의 건강이나 안전을 위한 필수적인 행위의 거부와 불이행이 의식적으로 이루어지는지,<br/>
                                &nbsp;&nbsp;심신의 손상으로 인해 나타나게 되는지를 확인하는 것이 자기방임의 구체적 형태와<br/>
                                &nbsp;&nbsp;원인을 파악하는 데 중요함<br/>
                                ∙ 최근 들어 심신의 손상으로 인한 대상자 이외에도 정신질환이나 인지적 손상과 관계없이<br/>
                                &nbsp;&nbsp;자발적 능력이 있음에도 불구하고 자기보호를 하지 않는 경우도 관찰되고 있음<br/>
                                * 심신의 장애가 없는 경우는 전체 사례 중 50~70%를 차지
                            </div>
                        </Bounce>
                        <div className='img'>
                            <Bounce right>
                                <img src="/images/under2_2.jpg"/>
                            </Bounce>
                        </div>
                    </div>

                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <Bounce top>
                        <div className='title'>
                            유사개념과 비교
                        </div>
                    </Bounce>
                    <div className="content">

                        <Bounce
                            bottom
                            duration={2000}
                        >
                            <div className='content-info2'>
                                <b>저장행동과 수집</b><br/>
                                ∙ 정상적 수집과 병리적 저장은 수집행동에 대한 동기와 수집품에 대한 태도, 계획성, 수집행동에 따른 감정에 차이가 있음<br/>
                                ∙ 수집은 물건 자체에 대한 심미적 열망과 개인적 성취감 및 만족감과 같은 개인적 동기 외에 사회적 주목과 전문적 과시(영예)와<br/>
                                &nbsp;&nbsp;같은 사회적 동기에 의해 작동함<br/>
                                ∙ 정상적 수집가들은 수집품을 타인에게 기꺼이 보여주고 자랑하며 그것에 관한 대화를 즐기며, 그 과정에서 느끼는 긍정적 감정이<br/>
                                &nbsp;&nbsp;수집행동을 강화하는 기제로 작동함<br/>
                                ∙ 병리적 저장은 주로 부정적 감정을 피하기 위한 목적으로 유발되며, 그들의 소유물을 타인이 보거나 만지는 것을 매우 싫어함<br/>
                            </div>
                        </Bounce>

                    </div>

                </Container>
            </div>
        </div>
    )
}

export default UnderStand2Component;