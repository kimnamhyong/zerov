import React, {useState} from "react";
import {Container} from "@mui/material";
import Bounce from "react-reveal/Bounce";
import Fade from "react-reveal/Fade";
import TabLayout from "@/component/TabLayout";

const Adance2Component = () => {
    const tabMenu = [
        {id : 1,menu:'사전답사'},
        {id : 2,menu:'견적 산출'},
        {id : 3,menu:'작업일정 확정'},
        {id : 4,menu:'서비스업체 선정'}
    ];
    const [index_no,setIndexNo] = useState(1);
    const onClickTab = (no) => {
        setIndexNo(no);
    }
    return (
        <div>
            <div className='service-content1 under1_back'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            사전 서비스 단계
                            <div className='subtitle'>
                                사전 서비스 준비 절차
                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            서비스 대상 가구를 발굴과 욕구조사를 하며,<br/>
                            정확한 원인 분석과 면담을 통해 가구를 선정합니다.

                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <TabLayout
                        index_no={index_no}
                        tabMenu={tabMenu}
                        onClickTab={onClickTab}
                    />
                    <div className='content'>
                        {
                            index_no===1?
                                <div className='content-info'>
                                    <Fade>
                                    1) 사전답사란?<br/>
                                        대상자의 주거를 직접 방문하여 대상 가구의 주거환경 개선 서비스의 구체적인 작업을 계획하고 전반적 작업일정을 구상하는 일<br/><br/>
                                    2) 사전답사 절차<br/>
                                        ∙ 저장위기가구를 담당하는 사례관리자를 통하여 대상 가구에 대한 정보 취득<br/>
                                        ∙ 사례관리자와 일정을 조율하여 대상 가구 방문일 확정<br/>
                                        ∙ 대상 가구를 방문하여 서비스 내용 관련 대상자와 협의 및 조율<br/>
                                        ∙ 작업환경 및 작업량 확인(현장 사진 촬영)<br/>
                                        ∙ 작업 인원, 작업 준비물, 협력 서비스업체, 주의사항 등 파악<br/>
                                        ∙ 견적서 작성<br/>
                                        ∙ 작업일정 확정<br/><br/>
                                    3) 서비스 개입 유형별 빈도<br/>
                                    ∙ 발굴 가구에 대한 서비스 개입 유형별 빈도 비중은 주거환경 개선 서비스, 정신과 의뢰, 통합사례관리, 공적부조 신청, 기타 서비스 순으로 높게 나타남
                                    </Fade>
                                </div>:null
                        }
                        {
                            index_no===2?
                                <div className='content-info'>
                                    <Fade>
                                        1) 견적서 작성<br/>
                                        ∙ 사전답사에서 획득한 정보의 정리<br/>
                                        ∙ 폐기물량과 폐기물 악취 및 재질 확인<br/>
                                        ∙ 폐기물 관련 사항을 체크하여 작업 인원 산출<br/>
                                        ∙ 필요 장비 및 준비물 확인<br/>
                                        ∙ 협력 서비스업체 확인<br/>
                                        ∙ 비용 산출 및 견적서 작성
                                    </Fade>
                                </div>:null
                        }
                        {
                            index_no===3?
                                <div className='content-info'>
                                    <Fade>

                                        1) 작업일정 확정 절차<br/>
                                        ∙ 사례관리자가 주축이 되어 대상 가구, 사례관리자, 폐기물처리업체, 재활용수거업체등과의 일정 조율<br/>
                                        ∙ 폐기물 및 재활용수거 지정요일 확인하여 작업일정 확정<br/>
                                        ∙ 사전답사시 확인된 작업물의 양에 따라 추가 장비(사다리차 등)가 필요하다고 판단되는 경우 작업일정에 맞추어 예약<br/>
                                    </Fade>
                                </div>:null
                        }
                        {
                            index_no===4?
                                <div className='content-info'>
                                    <Fade>

                                        1) 서비스 제공업체<br/>
                                        ∙ 주거환경개선 서비스 제공<br/>
                                        - 폐기물처리<br/>
                                        - 주거청소<br/>
                                        - 소독·방역<br/><br/>

                                        2) 협력 서비스업체<br/>
                                        ∙ 폐기물처리업체: 생활폐기물 처리 및 수거<br/>
                                        - 생활폐기물 처리업체 선정<br/>
                                        - 이삿짐센터 활용시 처리 과정의 효율성을 높일 수 있음<br/>
                                        ∙ 폐기물 처리 차량: 처리업체에 차량이 없을 경우 일반 개인용달 업체 또는 폐기물처리업체의 차량 섭외<br/>
                                        ∙ 재활용수거업체: 오염되지 않은 플라스틱 등의 재활용품 수거<br/>
                                        ∙ 집수리업체: 도배, 장판 등 집수리<br/>
                                        - 수리해야 할 경우를 대비하여 사전 섭외<br/>
                                        ∙ 수도배관설비업체: 폐기물처리 후 수도배관 공사<br/>
                                        ∙ 전기설비업체<br/>
                                        ∙ 하수도전문업체<br/>
                                        ∙ 사다리차업체<br/>
                                        ∙ 소독방역업체<br/><br/>

                                        3) 협력 서비스업체 선정시 검토사항<br/>
                                        ∙ 계약금액 조율 및 산출<br/>
                                        ∙ 증빙서류 확인<br/>
                                        - 사업자등록증<br/>
                                        - 작업사진: 작업 전/중/후 같은 각도에서 촬영<br/>
                                        - 저장위기 대상자 관련 교육 이수증<br/>
                                        - 유사사업 이력<br/>
                                    </Fade>
                                </div>:null
                        }

                    </div>
                </Container>
            </div>
        </div>
    )
}

export default Adance2Component;