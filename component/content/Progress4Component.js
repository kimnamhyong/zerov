import React, {useState} from "react";
import Bounce from "react-reveal/Bounce";
import {Container} from "@mui/material";
import Fade from "react-reveal/Fade";
import TabLayout from "@/component/TabLayout";

const Progress4Component = () => {
    const tabMenu = [
        {id : 1,menu:'작업준비물'},
        {id : 2,menu:'주거청소 절차 및 내용'},
        {id : 3,menu:'작업시 유의사항'}
    ];

    const [index_no,setIndexNo] = useState(1);
    const onClickTab = (no) => {
        setIndexNo(no);
    }
    return (
        <div>
            <div className='service-content1 progress-background'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            주거 청소
                            <div className='subtitle'>

                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            주거 청소
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <TabLayout
                        index_no={index_no}
                        tabMenu={tabMenu}
                        onClickTab={onClickTab}
                    />
                    <div className='content'>
                        {
                            index_no===1?
                                <div className='content-info2'>
                                    <Bounce top>
                                        1) 작업 준비물 리스트<br/>
                                        ∙ 청소도구<br/>
                                        - 밀대: 먼지 제거에 사용<br/>
                                        - 타올: 유리나 거울 청소시 사용<br/>
                                        - 걸레: 바닥이나 벽면, 가구나 물품 청소시 사용<br/>
                                        - 수세미: 오염된 타일 청소시 사용. 표면 손상 우려 유의<br/>
                                        - 고무장갑, 속장갑: 작업시 세제나 오염물로부터 신체 보호 목적<br/>
                                        - 스퀴지: 유리나 거울, 바닥 청소시 물기 제거에 사용<br/>
                                        ∙ 세제류<br/>
                                        - 중성 세제: 주방용 식기 세제로서 오염물질 제거시 사용<br/>
                                        - 기름 제거용 세제<br/>
                                        ➀ 기름때 제거시 사용<br/>
                                        ➁ 강알칼리성 세제를 사용했을 때 손상 및 변색 가능성이 있는지 사전 확인 필요<br/>
                                        ➂ 작업자의 신체에 직접 닿지 않도록 주의<br/>
                                        ➃ 고무장갑을 착용하여 화상 안전사고 예방(장갑 끝부분을 접어서 착용 권장)<br/>
                                        - 곰팡이 제거제: 주로 욕실이나 주방의 실리콘 부위에 있는 곰팡이 제거에 사용<br/>
                                        - 욕실용 세제: 욕실 천장과 바닥 및 변기 세척에 사용, 고압 세척기 이용<br/>
                                        - 요석 제거제: 변기 요석 제거시 사용<br/>
                                        - 광택제: 수전 표면이나 타일의 찌든 때 제거시 사용<br/>
                                        - 유리 세정제: 유리 세척시 사용<br/>
                                        - 스티커 제거제: 유리창, 싱크대, 냉장고 벽면 등에 붙어 있는 스티커 제거시 사용<br/>
                                        ∙ 장비류<br/>
                                        - 진공청소기: 벽면, 바닥, 먼지 제거시 사용<br/>
                                        - 스팀청소기: 찌든 때 청소 및 살균작업 필요시 사용<br/>
                                        - 자외선살균기: 청소 완료후 주방도구나 매트 등의 살균시 사용<br/>
                                        - 사다리: 높은 곳을 청소할 때 사용<br/>
                                        - 드라이버: 창틀 이물질 제거 및 배수구 커버 개방시 사용<br/>
                                        ∙ 개인 방역용품<br/>
                                        - 방진복: 오염된 외부 환경으로부터 작업자의 안전 도모<br/>
                                        - 방진마스크: 작업시 유해물질(미세먼지 등) 흡입 예방<br/>
                                        ∙ 기타용품<br/>
                                        - 다용도 봉투: 사용한 청소용품 수거시 사용<br/>
                                    </Bounce>

                                </div>:null
                        }
                        {
                            index_no===2?
                                <div className='content-info2'>
                                    <Fade>
                                        1) 주거공간 환경 파악<br/>
                                        ∙ 주거공간 확인<br/>
                                        ∙ 대상자와 사례관리자의 동의를 얻은 후 현장 사진 촬영<br/>
                                        ∙ 주거공간 내 벽지, 몰딩, 바닥 오염 상태 확인<br/>
                                        ∙ 사례관리자와 청소 범위에 대해 정확히 협의<br/>
                                        ∙ 대상자에게 청소작업과 복원 가능성에 대해 안내<br/><br/>

                                        2) 작업 준비 및 인원 구성<br/>
                                        ∙ 작업공간 확보<br/>
                                        - 작업 전 공간확보를 통해 안전관리<br/>
                                        ∙ 인원 구성 및 배치<br/>
                                        - 공간별 인력 배치<br/><br/>

                                        2) 주거공간 청소<br/>
                                        ∙ 공간별 청소<br/>
                                        - 방, 거실: 천장 및 벽면, 바닥, 가구, 조명, 창문, 창틀, 출입문 등<br/>
                                        - 주방: 천장 및 벽면, 주방가구, 조명, 렌지, 후드, 개수대, 주방용품 등<br/>
                                        - 화장실: 천장 및 벽면, 바닥, 욕조, 세면대, 샤워부스, 변기, 거울, 수납장 등<br/>
                                        - 베란다, 다용도실: 천장 및 벽면, 바닥, 창문, 창틀, 방충망, 배수구 등<br/>
                                        - 현관: 천장 및 벽면, 바닥, 현관문 등<br/>
                                        ∙ 위에서 아래로, 안쪽에서 바깥쪽으로 작업<br/><br/>

                                        3) 작업 마무리<br/>
                                        ∙ 청소 상태 재점검<br/>
                                        ∙ 가구, 가전, 용품과 비품 등 본래 위치로 배치<br/>
                                        ∙ 사례관리자(또는 주민센터 담당자)에게 작업 보고<br/>
                                        ∙ 서비스 완료 사진 촬영<br/>
                                    </Fade>
                                </div>:null
                        }
                        {
                            index_no===3?
                                <div className='content-info2'>
                                    <Fade>
                                        1) 미끄럼방지 작업화 착용<br/>
                                        2) 벌어짐 방지 장치가 설치된 안전사다리 사용<br/>
                                        3) 손목 및 팔까지 보호되는 보호구 착용<br/>
                                    </Fade>
                                </div>:null
                        }
                        {}
                    </div>
                </Container>
            </div>
        </div>
    )
}

export default Progress4Component;