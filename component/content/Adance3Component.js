import React, {useState} from "react";
import {Container} from "@mui/material";
import Bounce from "react-reveal/Bounce";
import Fade from "react-reveal/Fade";
import TabLayout from "@/component/TabLayout";

const Adance3Component = () => {
    const tabMenu = [
        {id : 1,menu:'현장 정보 교육'},
        {id : 2,menu:'사용 장비 교육'},
        {id : 3,menu:'안전관리'}
    ];
    const [index_no,setIndexNo] = useState(1);
    const onClickTab = (no) => {
        setIndexNo(no);
    }
    return (
        <div>
            <div className='service-content1 under1_back'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            사전 서비스 단계
                            <div className='subtitle'>
                                사전 교육 및 안전관리
                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            사전답사 후 견적 내용에 따라 작업 인력을 확보하고,<br/>
                            확보된 작업예정자에 대하여 현장 정보와 사용 장비 및 안전에 관한 사전 교육을 실시합니다.

                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <TabLayout
                        index_no={index_no}
                        tabMenu={tabMenu}
                        onClickTab={onClickTab}
                    />
                    <div className='content'>
                        {
                            index_no===1?
                                <div className='content-info'>
                                    <Fade>
                                        1) 현장 정보 교육 내용<br/>
                                        ∙ 사전답사 시 획득한 현장 정보(주거환경 특성, 작업량 등)의 전달<br/>
                                        ∙ 현장 사진 제공을 통한 실제 현장 정보의 공유<br/>
                                        ∙ 대상 가구를 위한 서비스 내용 및 서비스 절차<br/>
                                        ∙ 대상 가구를 위한 작업 준비물 및 작업시 유의사항<br/>
                                        ∙ 투입되는 협력 서비스업체의 유형과 역할<br/><br/>

                                        2) 현장 정보 교육 효과<br/>
                                        ∙ 서비스 제공시간 단축 등 서비스 효율성 증대<br/>
                                        ∙ 서비스 제공자의 안전사고 예방 효과
                                    </Fade>
                                </div>:null
                        }
                        {
                            index_no===2?
                                <div className='content-info'>
                                    <Fade>
                                        1) 사용 장비 교육 내용<br/>
                                        ∙ 착용 장비(복장 등)의 역할과 착용방법<br/>
                                        ∙ 폐기물 처리 작업 장비 및 연장의 기능과 사용방법<br/>
                                        ∙ 주거청소 작업 장비 및 연장의 기능과 사용방법<br/>
                                        ∙ 소독·방역 작업 장비 및 연장의 기능과 사용방법<br/>
                                        ∙ 작업별 장비 및 연장 사용시 안전수칙<br/><br/>

                                        2) 사용 장비 교육 효과<br/>
                                        ∙ 서비스 제공시간 단축 등 서비스 효율성 증대<br/>
                                        ∙ 서비스 제공자의 안전사고 예방 효과<br/>
                                        ∙ 장비 손상을 최소화하여 수리 및 교체 비용 절감
                                    </Fade>
                                </div>:null
                        }
                        {
                            index_no===3?
                                <div className='content-info'>
                                    <Fade>

                                        1) 장비 안전관리<br/>
                                        ∙ 착용 장비의 상태 확인<br/>
                                        ∙ 작업 장비의 작동 확인<br/>
                                        ∙ 작업 연장의 상태 확인<br/><br/>

                                        2) 서비스 제공자 안전관리<br/>
                                        ∙ 복장 및 장비의 준비<br/>
                                        ∙ 인원에 맞춘 작업 준비물의 완비<br/>
                                        ∙ 안전 장비 및 구급용품 준비<br/>
                                        ∙ 작업 현장의 환경과 공간 및 동선 확인<br/>
                                        ∙ 서비스 안전수칙 숙지<br/><br/>
                                    </Fade>
                                </div>:null
                        }

                    </div>
                </Container>
            </div>
        </div>
    )
}

export default Adance3Component;