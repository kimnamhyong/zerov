import React from "react";
import Bounce from "react-reveal/Bounce";
import {Container} from "@mui/material";

const After3Component = () => {
    return (
        <div>
            <div className='service-content1 after-background'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            서비스 중단 및 <br/>재발에 대한<br/> 재개입 서비스
                            <div className='subtitle'>

                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            서비스 중단 및 재발에 대한 재개입 서비스
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='progress-content2'>
                <Container>
                    <Bounce top>
                        <ul>
                            <li>
                                <div className="title">가. 중단가구에 대한 직·간접 (재)개입 서비스</div><br/>
                                1) 대상자가 서비스 중단에 대한 의사를 표현한 경우<br/>
                                ∙ 서비스 중단 후 사례관리자(또는 주민센터 담당자)에게 보고<br/>
                                ∙ 사례관리자의 상담 및 재사정<br/>
                                ∙ 대상자의 동의 후 주거환경개선 서비스 재실시<br/><br/>

                                2) 서비스가 대상자에게 적절하지 않거나 서비스 제공 목적에 어긋날 경우<br/>
                                ∙ 사례관리자의 상담<br/>
                                ∙ 서비스 연계 및 이관<br/><br/>

                                3) 대상자의 부적절한 서비스 요구가 있을 경우<br/>
                                ∙ 서비스 중단 후 사례관리자(또는 주민센터 담당자)에게 보고<br/>
                                ∙ 사례관리자의 상담<br/><br/>


                            </li>
                            <li>
                                <div className="title">나. 재발가구에 대한 직·간접 (재)개입 서비스</div><br/>
                                1) 대상자 교육 재실시<br/>
                                2) 사례관리자의 상담 및 재사정<br/>
                                3) 주거환경개선 서비스 재실시<br/>
                            </li>
                        </ul>
                    </Bounce>
                </Container>
            </div>
        </div>
    )
}

export default After3Component;