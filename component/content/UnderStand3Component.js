import React from "react";
import Bounce from 'react-reveal/Bounce';
import {Container} from "@mui/material";

const UnderStand3Component = () => {
    return (
        <div>
            <div className='service-content1 under1_back'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            저장행동의 이해
                            <div className='subtitle'>
                                저장위기가구의 진단
                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            저장위기가구는 정신적 질환인 저장장애와 생활기능 약화에 따른 환경불결 상태를 보이는 자기방임을 모두 포함한다. 이에 두 유형의 저장위기가구를 발굴해내기 위해서는 정서 진단과 행동 진단을 함께 사용할 것을 제안한다.
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <Bounce top>
                        <div className='title'>
                            정서 진단
                        </div>
                        <div className='subtitle'>
                            저장위기가구의 정서 진단에 대해 알아보겠습니다.
                        </div>
                    </Bounce>
                    <div className="content">
                        <div className='img'>
                            <Bounce left>
                                <img src="/images/under2_1.jpg"/>
                            </Bounce>
                        </div>
                        <Bounce
                            right
                            duration={2000}
                        >
                            <div className='content-info'>
                                1) 저장위기가구 대상자의 저장강박 증상을 진단<br/>
                                2) 저장강박으로 인해 저장행동을 보이는 대상자 파악이 가능<br/>
                                3) 저장강박 척도를 사용하여 대상자의 저장강박 정도를 측정
                            </div>
                        </Bounce>
                    </div>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <Bounce top>
                        <div className='title'>
                            행동 진단
                        </div>
                        <div className='subtitle'>
                            행동 진단이 어떤 것이 있는지 알아보겠습니다.
                        </div>
                    </Bounce>
                    <div className="content">

                        <Bounce
                            left
                            duration={2000}
                        >
                            <div className='content-info'>
                                1) 저장위기가구 대상자의 주거 생활환경의 불결상태를 진단<br/>
                                2) 저장강박으로 인한 저장행동 대상자와 저장강박 증상이 없더라도<br/>
                                &nbsp;&nbsp;&nbsp;자기방임으로 인해 저장행동을 보이는 대상자 모두 파악 가능<br/>
                                3) 자기방임 척도와 주거환경 내 쓰레기나 잡동사니의 총량을 계량적으로 측정하거나<br/>
                                &nbsp;&nbsp;&nbsp;이미지 척도(Clutter Image Rating Scale)를 사용하여 저장행동 정도를 측정
                            </div>
                        </Bounce>
                        <div className='img'>
                            <Bounce right>
                                <img src="/images/under2_2.jpg"/>
                            </Bounce>
                        </div>
                    </div>

                </Container>
            </div>

        </div>
    )
}

export default UnderStand3Component;