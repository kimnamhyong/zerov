import React from "react";
import Bounce from 'react-reveal/Bounce';

const Service4Component = () => {
    return (
        <div>
            <div className='titles'>서비스 운영체계</div>
            <div className='service-procedure'>

                <Bounce
                    center
                    duration={500}
                >
                    <img src="/images/service_procedure.png"/>
                </Bounce>

            </div>
        </div>
    )
}

export default Service4Component;