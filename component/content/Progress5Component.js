import React from "react";
import Bounce from "react-reveal/Bounce";
import {Container} from "@mui/material";

const Progress5Component = () => {
    return (
        <div>
            <div className='service-content1 progress-background'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            소독·방역
                            <div className='subtitle'>

                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            소독·방역
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='progress-content1'>
                <Container>
                    <Bounce top>
                        <ul>
                            <li>
                                <div className="title">가. 작업 준비물</div><br/>
                                1) 작업 준비물 리스트<br/>
                                ∙ 전신보호복<br/>
                                ∙ 고무장갑, 1회용 장갑<br/>
                                ∙ 안면보호구<br/>
                                ∙ 고글<br/>
                                ∙ 의료용 폐기물 봉투<br/>
                            </li>
                            <li>
                                <div className="title">나. 소독·방역 서비스 절차 및 내용</div><br/>
                                1) 주거공간 환경 파악<br/>
                                ∙ 주거공간 확인<br/>
                                ∙ 표면 소독이 필요한 물건 등 확인<br/><br/>

                                2) 작업 준비 및 인원 구성<br/>
                                ∙ 작업공간 확보<br/>
                                - 작업 전 공간확보를 통해 안전관리<br/>
                                ∙ 인원 구성 및 배치<br/>
                                - 공간별 인력 배치<br/><br/>

                                3) 주거공간 소독·방역<br/>
                                ∙ 주거공간 소독<br/>
                                ∙ 접촉이 많은 물건 소독<br/>
                                ∙ 주거공간 내 특정 부분(손잡이, 스위치 등)의 표면 소독<br/><br/>

                                4) 작업 마무리<br/>
                                ∙ 사례관리자(또는 주민센터 담당자)에게 작업 보고<br/>
                            </li>
                            <li>
                                <div className="title">다. 작업시 유의사항</div><br/>
                                1) 소독대상물의 변색 등 유의<br/>
                                2) 소독 중 환기를 위해 창문 개방<br/>
                                3) 소독 효과를 위해 소독 전 물건 등의 표면 이물질 제거<br/>
                                4) 소독시 발생한 폐기물은 의료용 폐기물 봉투에 담아 배출<br/>
                                5) 소독 완료 후 청소도구 소독 보관<br/>
                            </li>
                        </ul>
                    </Bounce>
                </Container>
            </div>
        </div>
    )
}

export default Progress5Component;