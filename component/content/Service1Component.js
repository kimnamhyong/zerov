import React from "react";
import Bounce from 'react-reveal/Bounce';
import {Container} from "@mui/material";

const Service1Component = () => {
    return (
        <div>
            <div className='service-content1 service1-background'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            배경
                            <div className='subtitle'>
                                저장 위기에 배경은?
                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            1) 지역 내 저장위기가구가 지속적으로 증가함에 따라, 저장위기가구 대상자들의 심리적·환경적 어려움을 해결하고 이를 뒷받침하는 견고한 지역 안전망 형성을 위한 새로운
                            개입 방안의 필요성 증대<br/>
                            2) 기존 사례관리서비스 절차로는 개입의 한계가 있으며, 저장위기가구에 대한 체계적인 접근과 증상 완화 및 재발 방지를 위해서는 주거환경개선만이 아닌 대상자 중심의
                            단계별 개입 방안과 통합적 운영 지침의 필요성 인식
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <Bounce top>
                        <div className='title'>
                            목적
                        </div>
                        <div className='subtitle'>
                            문제해결을 위한 목적은?
                        </div>
                    </Bounce>
                    <div className="content">
                        <div className='img'>
                            <Bounce left>
                            <img src="/images/service_1_2.jpg"/>
                            </Bounce>
                        </div>
                        <Bounce
                            right
                            duration={2000}
                        >
                        <div className='content-info'>
                            1) 저장위기가구에 대한 사회적 관심과 우려에 따른 실천적 방안으로 개인적·사회적 문제를 겪고 있는 저장위기가구를 대상으로 주거환경정비 및 개선 서비스를 제공하여 대상자의 심리적· 환경적 어려움을 해결하고 견고한 지역안전망 형성에 기여<br/><br/>
                            2) 저장위기가구의 주거환경개선을 중심으로 하되, 서비스 제공 과정 전반에 걸쳐 대상자 중심의 통합적 운영 및 관리 관점에서 단계별 개입을 통해 대상자의 증상 완화 및 재발 방지를 통한 서비스 만족도 제고<br/><br/>
                            3) 주거환경정비 서비스 사업을 수행한 경험을 바탕으로 저장위기가구의 문제해결을 위한 매뉴얼을 개발하여 저장위기가구에 대한 서비스 시행의 표준관리지침으로 활용<br/>
                        </div>
                        </Bounce>
                    </div>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <Bounce top>
                        <div className='title'>
                            기대효과
                        </div>
                        <div className='subtitle'>
                            저장위기 지원에 대한 기대효과는?
                        </div>
                    </Bounce>
                    <div className="content">

                        <Bounce
                            bottom
                            duration={2000}
                        >
                            <div className='content-info'>
                                1) 대상자의 개인적·사회적 문제해결을 위한 체계화된 주거환경개선 서비스 모델 제시<br/><br/>
                                2) 저장위기가구 지원을 위한 운영매뉴얼 개발 및 검증을 통한 사업 체계화<br/><br/>
                                3) 주거환경개선 서비스 제공업체를 주체로 한 실천가이드 개발로 사업의 범위 확대 및
                                운영의 수월성 확보<br/><br/>
                                4) 저장위기가구에 대한 체계적인 접근을 통해 실제 현장에서의 적용 및 활용도 제고<br/><br/>
                            </div>
                        </Bounce>
                        <div className='img'>
                            <Bounce top>
                                <img src="/images/service_1_3.jpg"/>
                            </Bounce>
                        </div>
                    </div>
                </Container>
            </div>
        </div>
    )
}

export default Service1Component;