import React from "react";
import Bounce from "react-reveal/Bounce";
import {Container} from "@mui/material";

const After1Component = () => {
    return (
        <div>
            <div className='service-content1 after-background'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            대상가구 교육 및 입주 관리
                            <div className='subtitle'>

                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            대상가구 교육 및 입주 관리
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='progress-content2'>
                <Container>
                    <Bounce top>
                        <ul>
                            <li>
                                <div className="title">가. 대상자 교육</div><br/>
                                1) 문제해결 능력 함양을 위한 교육<br/>
                                ∙ 정리 능력과 결정 능력을 높이기 위한 실천 교육<br/>
                                ∙ 버리는 연습에 대한 교육<br/>
                                - 정신적으로 부담이 덜 되는 부분부터 하나씩 정리하기<br/>
                                - 방, 특정 공간, 물건 등 주제를 정해서 하나씩 정리해 나가기<br/>
                                - 쉬운 것부터 시작해서 어려운 것으로 나아가기<br/>
                                - 무엇을 보관하고 버릴지에 대한 의사결정 해보기<br/><br/>

                                2) 서비스 제공에 대한 설문조사 실시<br/>
                                ∙ 서비스에 대한 만족도 평가<br/>
                                ∙ 향후 주거공간 관리에 대한 의지 확인<br/>
                                ∙ 사후 지원서비스에 대한 의견 청취<br/><br/>

                                3) 서비스 진행에 대한 정보 제공<br/>
                                ∙ 서비스 제공 전 서비스 진행에 관한 설명<br/>
                                ∙ 서비스 제공 기간 중에 제공되고 있는 서비스에 대한 설명<br/>
                                ∙ 서비스 제공 후 제공된 서비스에 대한 설명<br/>


                            </li>
                            <li>
                                <div className="title">나. 대상자 입주 관리</div><br/>
                                1) 입주 가이드 제공<br/>
                                ∙ 입주 날짜<br/>
                                ∙ 입주 절차<br/>
                                ∙ 확인사항<br/>
                                ∙ 주의사항<br/><br/>

                                2) 입주 전 사전점검<br/>
                                ∙ 청소 상태 점검<br/>
                                ∙ 보수 상태 점검<br/>
                                ∙ 대상자 개인 물품 확인
                            </li>
                        </ul>
                    </Bounce>
                </Container>
            </div>
        </div>
    )
}

export default After1Component;