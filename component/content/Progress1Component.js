import React from "react";
import Bounce from "react-reveal/Bounce";
import {Container} from "@mui/material";

const Progress1Component = () => {
    return (
        <div>
            <div className='service-content1 progress-background'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            서비스 유형별<br/>절차
                            <div className='subtitle'>

                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            유형별로 절차를 설명
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='progress-content1'>
                <Container>
                    <Bounce top>
                        <ul>
                            <li>
                                <div className="title">가. 폐기물 처리 서비스</div><br/>
                                1) 폐기물 확인<br/>
                                2) 작업 준비 및 인원 구성<br/>
                                3) 폐기물 선별<br/>
                                4) 폐기물 분류 및 포장<br/>
                                5) 폐기물 이동<br/>
                                6) 작업 마무리
                            </li>
                            <li>
                                <div className="title">나. 주거청소 서비스</div><br/>
                                1) 주거공간 환경 파악<br/>
                                2) 작업 준비 및 인원 구성<br/>
                                2) 주거공간 청소<br/>
                                3) 작업 마무리
                            </li>
                            <li>
                                <div className="title">다. 소독·방역 서비스</div><br/>
                                1) 주거공간 환경 파악<br/>
                                2) 작업 준비 및 인원 구성<br/>
                                3) 주거공간 소독·방역<br/>
                                4) 작업 마무리
                            </li>
                        </ul>
                    </Bounce>
                </Container>
            </div>
        </div>
    )
}

export default Progress1Component;