import React from "react";
import Bounce from 'react-reveal/Bounce';

const Service2Component = () => {
    return (
        <div>
            <div className='titles'>서비스 절차</div>
            <div className='service-chart'>

                <Bounce
                    center
                    duration={500}
                >
                    <img src="/images/service_chart.png"/>
                </Bounce>

            </div>
        </div>
    )
}

export default Service2Component;