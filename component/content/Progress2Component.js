import React from "react";
import Bounce from "react-reveal/Bounce";
import {Container} from "@mui/material";

const Progress2Component = () => {
    return (
        <div>
            <div className='service-content1 progress-background'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            서비스 제공 단계별<br/>실행 가이드
                            <div className='subtitle'>

                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            서비스 제공 단계별 기능 및 역할
                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='progress-content2'>
                <Container>
                    <Bounce top>
                        <ul>
                            <li>
                                <div className="title">1) 폐기물 처리 서비스</div><br/>
                                ∙ 폐기물 선별, 분류 및 배출<br/>
                                ∙ 처리 과정에서 대상자와의 원활한 소통<br/>
                                ∙ 대상자 주거안정과 활동의 자유 확보<br/>
                            </li>
                            <li>
                                <div className="title">2) 주거청소 서비스</div><br/>
                                ∙ 주거공간 내 이물질 및 오염제거<br/>
                                ∙ 주거 내에서 이루어지는 작업과 능률 향상<br/>
                                ∙ 쾌적한 주거공간 유지
                            </li>
                            <li>
                                <div className="title">3) 소독방역 서비스</div><br/>
                                ∙ 살균 및 해충 방제<br/>
                                ∙ 유해물질 제거를 통한 감염병 예방<br/>
                                ∙ 대상자와 이웃의 건강한 주거환경 조성
                            </li>
                            <li>
                                <div className="title">4) 사후 서비스</div><br/>
                                ∙ 서비스 만족도 관리와 유지<br/>
                                ∙ 대상자의 상황 및 변화 상태 점검<br/>
                                ∙ 재발 방지 및 대상자의 문제해결 능력 함양
                            </li>
                        </ul>
                    </Bounce>
                </Container>
            </div>
        </div>
    )
}

export default Progress2Component;