import React, {useState} from "react";
import {Container} from "@mui/material";
import Bounce from "react-reveal/Bounce";
import Fade from "react-reveal/Fade";
import TabLayout from "@/component/TabLayout";

const Adance4Component = () => {
    const tabMenu = [
        {id: 1, menu: '사전답사 준비물'},
        {id: 2, menu: '견적서 작성'}
    ];
    const [index_no, setIndexNo] = useState(1);
    const onClickTab = (no) => {
        setIndexNo(no);
    }
    return (
        <div>
            <div className='service-content1 under1_back'>
                <Container>
                    <div className='title'>
                        <Bounce left>
                            사전 서비스 단계<br/><br/>
                            <div className='subtitle'>
                                사전 교육 및 안전관리
                            </div>
                        </Bounce>
                    </div>
                    <Bounce
                        bottom
                        duration={2000}
                    >
                        <div className='content'>
                            서비스 대상 가구를 발굴과 욕구조사를 하며,<br/>
                            정확한 원인 분석과 면담을 통해 가구를 선정합니다.

                        </div>
                    </Bounce>
                </Container>
            </div>
            <div className='service-content1-2'>
                <Container>
                    <TabLayout
                        index_no={index_no}
                        tabMenu={tabMenu}
                        onClickTab={onClickTab}
                    />
                    <div className='content'>
                        {
                            index_no === 1 ?
                                <div className='content-info2'>
                                    <Fade>
                                        1) 사전답사 준비물 목록<br/>
                                        ∙ 작업별 체크리스트<br/>
                                        ∙ 현장 촬영을 위한 장비<br/>
                                        ∙ 위생덧신과 코팅장갑<br/>
                                        ∙ 대상자 인적사항 차트<br/>
                                        ∙ 안전모<br/>
                                        ∙ 안전화<br/>
                                        ∙ 고글, 방진복<br/>
                                        ∙ 장화<br/>
                                        ∙ 메모장<br/>
                                        ∙ 기타 장비: 랜턴 등<br/><br/>

                                        2) 사전답사 준비물 사진
                                        <div className="mobile-image">
                                            <img src="/images/advance3_1.jpeg"/><br/>
                                            <b>안전모</b><br/><br/>
                                            <img src="/images/advance3_2.jpeg"/><br/>
                                            <b>장화</b><br/><br/>
                                            <img src="/images/advance3_3.jpeg"/><br/>
                                            <b>방진마스크</b><br/><br/>
                                            <img src="/images/advance3_4.jpeg"/><br/>
                                            <b>위생덧신</b><br/><br/>
                                            <img src="/images/advance3_5.jpeg"/><br/>
                                            <b>헤드램프</b><br/><br/>
                                            <img src="/images/advance3_6.jpeg"/><br/>
                                            <b>메모장</b><br/><br/>
                                            <img src="/images/advance3_7.jpeg"/><br/>
                                            <b>촬영장비</b><br/><br/>
                                            <img src="/images/advance3_8.jpeg"/><br/>
                                            <b>안전화</b><br/><br/>
                                            <img src="/images/advance3_9.jpeg"/><br/>
                                            <b>고글</b><br/><br/>
                                            <img src="/images/advance3_10.jpeg"/><br/>
                                            <b>방진복</b><br/><br/>
                                        </div>
                                        <table className="image-table">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <img src="/images/advance3_1.jpeg"/><br/>
                                                    <b>안전모</b>
                                                </td>
                                                <td>
                                                    <img src="/images/advance3_2.jpeg"/><br/>
                                                    <b>장화</b>
                                                </td>
                                                <td>
                                                    <img src="/images/advance3_3.jpeg"/><br/>
                                                    <b>방진마스크</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="/images/advance3_4.jpeg"/><br/>
                                                    <b>위생덧신</b>
                                                </td>
                                                <td>
                                                    <img src="/images/advance3_5.jpeg"/><br/>
                                                    <b>헤드램프</b>
                                                </td>
                                                <td>
                                                    <img src="/images/advance3_6.jpeg"/>
                                                    <img src="/images/advance3_7.jpeg"/>
                                                    <br/>
                                                    <b>메모장</b> <b>촬영장비</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="/images/advance3_8.jpeg"/><br/>
                                                    <b>안전모</b>
                                                </td>
                                                <td>
                                                    <img src="/images/advance3_9.jpeg"/><br/>
                                                    <b>장화</b>
                                                </td>
                                                <td>
                                                    <img src="/images/advance3_10.jpeg"/><br/>
                                                    <b>방진마스크</b>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </Fade>
                                </div> : null
                        }
                        {
                            index_no === 2 ?
                                <div className='content-info2'>
                                    <Fade>
                                        1) 사용 장비 교육 내용<br/>
                                        ∙ 착용 장비(복장 등)의 역할과 착용방법<br/>
                                        ∙ 폐기물 처리 작업 장비 및 연장의 기능과 사용방법<br/>
                                        ∙ 주거청소 작업 장비 및 연장의 기능과 사용방법<br/>
                                        ∙ 소독·방역 작업 장비 및 연장의 기능과 사용방법<br/>
                                        ∙ 작업별 장비 및 연장 사용시 안전수칙<br/><br/>

                                        2) 사용 장비 교육 효과<br/>
                                        ∙ 서비스 제공시간 단축 등 서비스 효율성 증대<br/>
                                        ∙ 서비스 제공자의 안전사고 예방 효과<br/>
                                        ∙ 장비 손상을 최소화하여 수리 및 교체 비용 절감
                                    </Fade>
                                </div> : null
                        }
                        {
                            index_no === 3 ?
                                <div className='content-info2'>
                                    <Fade>

                                        1) 장비 안전관리<br/>
                                        ∙ 착용 장비의 상태 확인<br/>
                                        ∙ 작업 장비의 작동 확인<br/>
                                        ∙ 작업 연장의 상태 확인<br/><br/>

                                        2) 서비스 제공자 안전관리<br/>
                                        ∙ 복장 및 장비의 준비<br/>
                                        ∙ 인원에 맞춘 작업 준비물의 완비<br/>
                                        ∙ 안전 장비 및 구급용품 준비<br/>
                                        ∙ 작업 현장의 환경과 공간 및 동선 확인<br/>
                                        ∙ 서비스 안전수칙 숙지<br/><br/>
                                    </Fade>
                                </div> : null
                        }

                    </div>
                </Container>
            </div>
        </div>
    )
}

export default Adance4Component;