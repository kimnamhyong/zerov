import React, {useState} from "react";
import {Button, Tab, Tabs} from "@mui/material";
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import PhoneIcon from '@mui/icons-material/Phone';
import FavoriteIcon from '@mui/icons-material/Favorite';
import PersonPinIcon from '@mui/icons-material/PersonPin';
const MainContactComponent = () => {
    const [value, setValue] = useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    return (
        <div className='main-contact'>
            <div className='main-content'>
                <div className='infomation'>
                    <div className='title'>문의안내</div>
                    <div className='content'>
                        <span><b>365일 24시간</b></span> /
                        <span> 전화,문자상담!</span>
                    </div>
                    <div className='content2'>
                        <span><b>연중무휴</b></span> /
                        <span>야간작업가능!</span>
                    </div>
                </div>
                <div className='contact-info'>
                    <ul>
                        <li className='title'>직통 전화</li>
                        <li> | </li>
                        <li className='tel-number'>051-5555-5555</li>
                        <li>
                            <Button variant='outlined' startIcon={<LocalPhoneIcon/>}>
                                문의하기
                            </Button>
                        </li>
                    </ul>
                    <ul>
                        <li className='title'>직통 전화</li>
                        <li> | </li>
                        <li className='tel-number'>051-5555-5555</li>
                        <li>
                            <Button variant='outlined' startIcon={<LocalPhoneIcon/>}>
                                문의하기
                            </Button>
                        </li>
                    </ul>
                    <ul>
                        <li className='title'>직통 전화</li>
                        <li> | </li>
                        <li className='tel-number'>051-5555-5555</li>
                        <li>
                            <Button variant='outlined' startIcon={<LocalPhoneIcon/>}>
                                문의하기
                            </Button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default MainContactComponent;