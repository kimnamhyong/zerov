import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import { Pagination } from "swiper";
const MainSwiperComponent = () => {
    return (
        <div style={{marginTop:'70px'}}>
            <Swiper
                pagination={{
                    clickable: true,
                }}
                modules={[Pagination]}
                className="mySwiper">
                <SwiperSlide>
                    <img src="/images/main1.jpg" className='main-swiper-image'/>
                    <div className='main-swiper-title'>정성을 다하는 제로브이</div>
                    <div className='main-swiper-content'>
                        저장강박증, 쓰레기집 <br/>
                        <b>완벽한 청소와 관리</b>
                    </div>
                </SwiperSlide>
                <SwiperSlide>
                    <img src="/images/main2.jpg" className='main-swiper-image'/>
                    <div className='main-swiper-title'>
                        언제 어디서나 작업이 가능한 청소
                    </div>
                    <div className='main-swiper-content'>
                        감당하기 어려운 집 쓰레기, 방치된 쓰레기를<br/>
                        <b>빠르고 깔끔하게 청소</b>

                    </div>
                </SwiperSlide>
                <SwiperSlide>
                    <img src="/images/main3.jpg" className='main-swiper-image'/>
                    <div className='main-swiper-title'>
                        주기적으로 관리해주는 서비스
                    </div>
                    <div className='main-swiper-content'>
                        우울증/ 고독사 / 저장강박증을 <br/>
                        <b>주기적으로 관리해 주는 서비스</b>
                    </div>
                </SwiperSlide>
            </Swiper>
        </div>
    )
}

export default MainSwiperComponent;