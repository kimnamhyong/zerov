import React from "react";
import {Button} from "@mui/material";

const MainServiceComponent = () => {
    return (

        <div className='main-service'>
            <h3>제로브이의</h3>
            <h2>서비스</h2>
            <div>
                <ul>
                    <li>
                        <img src='/images/trash_icon.png'/><br/>
                        <div className='title'>쓰레기 분리</div>
                        <div className='content'>쓰레기 청소</div>
                    </li>
                    <li>
                        <img src='/images/home_icon.png'/><br/>
                        <div className='title'>집 청소</div>
                        <div className='content'>쓰레기 청소</div>
                    </li>
                    <li>
                        <img src='/images/trash_icon.png'/><br/>
                        <div className='title'>소독/살균</div>
                        <div className='content'>쓰레기 청소</div>
                    </li>
                    <li>
                        <img src='/images/trash_icon.png'/><br/>
                        <div className='title'>쓰레기 분리</div>
                        <div className='content'>쓰레기 청소</div>

                    </li>
                    <li>
                        <img src='/images/home_icon.png'/><br/>
                        <div className='title'>집 청소</div>
                        <div className='content'>쓰레기 청소</div>
                    </li>
                    <li>
                        <img src='/images/trash_icon.png'/><br/>
                        <div className='title'>소독/살균</div>
                        <div className='content'>쓰레기 청소</div>
                    </li>
                </ul>
            </div>
            <div className='main-center'>
                <div className='main-center-back'>
                </div>
                <div className='main-center-content'>
                    <div className='title1'>저장강박증 전문 특수 청소</div>
                    <div className='title2'>제로브이</div>
                    <div className='line'></div>
                    <button className='btn btn-round-line btn-over'>작성후기</button>
                </div>
            </div>
        </div>
    )
}

export default MainServiceComponent;