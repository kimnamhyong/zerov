import React from "react";

const CopyrightComponent = () => {
    return (
        <div className='copyright'>
            <div className='copyright-content'>
                <div className='compony-info'>
                    <div className='logo'>제로브이 로고</div>
                    <div className='compony-infomation'>
                        <div>제로브이</div>
                        사업자등록번호 : 000-00-00000 | 부산광역시 해운대구 반송2동...<br/>
                        TEL : 1500-0000 | HP: 010-0000-0000 | E-mail : kkk@mail.net
                    </div>
                </div>
                <div className='agree'>
                    이용약관 개인정보처리방침
                </div>
                <div className='copyright-year'>
                    Copyright ⓒ 2023 제로브이 All rights reserved.
                </div>
            </div>
        </div>
    );
}

export default CopyrightComponent;