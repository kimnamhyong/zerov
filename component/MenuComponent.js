import React from "react";
import PcMenuComponent from "../component/PcMenuComponent";
import MobileMenuComponent from "../component/MobileMenuComponent";
import {Box} from "@mui/material";

const MenuComponent = ({user,title}) => {
    return (
        <div>
            <PcMenuComponent/>
            <MobileMenuComponent user={user} title={title}/>
        </div>
    )
}

export default MenuComponent;