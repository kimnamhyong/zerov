import React, {useState} from "react";
import Link from "next/link";

const PcMenuComponent = () => {
    const [sub_no,setSubNo] = useState(0);
    const onMouseOverSubMenu = (no) =>{
        setSubNo(no);
    }
    return (
        <div className='desktop-menu'>
            {/* 좌측 메뉴 시작 */}
            <div className='left-menu'>
                <ul>
                    <li><img src="/images/logo.png" width='100'/></li>
                    <li
                        onMouseOver={()=>{onMouseOverSubMenu(1)}}
                        onMouseOut={()=>{setSubNo(0)}}>
                        <Link href="/content/service1">서비스 개요</Link>
                        <div className={sub_no===1?'submenu-open':'submenu-close'}>
                            <ul>
                                <li><Link href="/content/service1">서비스 목적</Link></li>
                                <li><Link href="/content/service2">서비스 절차</Link></li>
                                <li><Link href="/content/service3">서비스 주요내용</Link></li>
                                <li><Link href="/content/service4">서비스 운영체제</Link></li>
                            </ul>
                        </div>
                    </li>
                    <li
                        onMouseOver={()=>{onMouseOverSubMenu(2)}}
                        onMouseOut={()=>{setSubNo(0)}}>
                        <Link href="/content/understand1">저장행동의 이해</Link>
                        <div className={sub_no===2?'submenu-open':'submenu-close'}>
                            <ul>
                                <li><Link href="/content/understand1">저장강박과 자기방임</Link></li>
                                <li><Link href="/content/understand2">저장강박과 자기방임의 특성</Link></li>
                                <li><Link href="/content/understand3">저장위기가구의 진단</Link></li>
                                <li><Link href="/content/understand4">관련 주요 질환</Link></li>
                            </ul>
                        </div>
                    </li>
                    <li
                        onMouseOver={()=>{onMouseOverSubMenu(3)}}
                        onMouseOut={()=>{setSubNo(0)}}>
                        <Link href="/content/advance1">사전 서비스 단계</Link>
                        <div className={sub_no===3?'submenu-open':'submenu-close'}>
                            <ul>
                                <li><Link href="/content/advance1">서비스 대상 가구 선정</Link></li>
                                <li><Link href="/content/advance2">서비스 준비 절차</Link></li>
                                <li><Link href="/content/advance3">사전 교육 및 안전관리</Link></li>
                                <li><Link href="/content/advance4">사전 준비물 점검</Link></li>
                            </ul>
                        </div>
                    </li>
                    <li
                        onMouseOver={()=>{onMouseOverSubMenu(4)}}
                        onMouseOut={()=>{setSubNo(0)}}>
                        <Link href="/content/progress1">서비스 진행 단계</Link>
                        <div className={sub_no===4?'submenu-open':'submenu-close'}>
                            <ul>
                                <li><Link href="/content/progress1">서비스 유형별 절차</Link></li>
                                <li><Link href="/content/progress2">서비스 제공 단계별 실행 가이드</Link></li>
                                <li><Link href="/content/progress3">폐기물 처리</Link></li>
                                <li><Link href="/content/progress4">주거청소</Link></li>
                                <li><Link href="/content/progress5">소독·방역</Link></li>
                                <li><Link href="/content/progress6">상황 발생에 대한 예방 및 관리대책</Link></li>
                            </ul>
                        </div>
                    </li>
                    <li
                        onMouseOver={()=>{onMouseOverSubMenu(5)}}
                        onMouseOut={()=>{setSubNo(0)}}>
                        사후 서비스 단계
                        <div className={sub_no===5?'submenu-open':'submenu-close'}>
                            <ul>
                                <li><Link href="/content/after1">대상가구 교육 및 입주 관리</Link></li>
                                <li><Link href="/content/after2">서비스 종결 점검</Link></li>
                                <li><Link href="/content/after3">서비스 중단 및 재발에 대한 재개입 서비스</Link></li>
                                <li><Link href="/content/after4">사후 지원 서비스</Link></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            {/* 좌측 메뉴 끝 */}
            {/* 우측 메뉴 시작 */}
            <div className='right-menu'>

            </div>
            {/* 우측 메뉴 끝 */}
        </div>
    );
}

export default PcMenuComponent;