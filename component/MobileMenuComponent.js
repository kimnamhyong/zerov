import React, {useState} from 'react';
import {AppBar, Drawer, Toolbar, Typography} from "@mui/material";
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Link from "next/link";
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
const MobileMenuComponent = ({user,title,}) => {
    const [open,setOpen] = useState(false);
    const [menuNo,setMenuNo] = useState(0);
    //메뉴 클릭시
    const onClickMenu = (no) => {
        setMenuNo(no);
    }

    //드로우 메뉴 열기
    const onClickMenuOpen = () => {
        setOpen(true);
    }
    //드로우 메뉴 닫기
    const onClickMenuClose = () => {
        setOpen(false);
        setMenuNo(0);
    }
    return (
        <div className='mobile-menu'>
            <AppBar>
                <Toolbar>
                    <IconButton
                        edge="start"
                        size='large'
                        onClick={onClickMenuOpen}
                    >
                        <MenuIcon className='menubar'/>
                    </IconButton>
                    <Typography className='menu-title'>
                        {title}
                    </Typography>
                </Toolbar>
            </AppBar>
            {/* 드로우 메뉴 시작 */}
            <Drawer variant="persistent" open={open}>

                <div className='mobile-draw-menu'>
                    <div className='mobile-sign-menu'>

                    </div>
                    <div>
                        <ul>
                            <li onClick={
                                ()=> {
                                    onClickMenu(1);
                                }
                            }>
                                {
                                    menuNo === 1 ?<KeyboardArrowDownIcon/>:<KeyboardArrowRightIcon/>
                                }

                                <a>서비스 개요</a>
                                <div className={menuNo===1?'sub-menu-open':'sub-menu'}>
                                    <Link href="/content/service1">서비스 목적</Link><br/>
                                    <Link href="/content/service2">서비스 절차</Link><br/>
                                    <Link href="/content/service3">서비스 주요내용</Link><br/>
                                    <Link href="/content/service4">서비스 운영체제</Link>
                                </div>
                            </li>
                            <li onClick={
                                ()=> {
                                    onClickMenu(2);
                                }
                            }>
                                {
                                    menuNo === 2 ?<KeyboardArrowDownIcon/>:<KeyboardArrowRightIcon/>
                                }
                                저장행동의 이해
                                <div className={menuNo===2?'sub-menu-open':'sub-menu'}>
                                    <Link href="/content/understand1">저장강박과 자기방임</Link><br/>
                                    <Link href="/content/understand2">저장강박과 자기방임의 특성</Link><br/>
                                    <Link href="/content/understand3">저장위기가구의 진단</Link><br/>
                                    <Link href="/content/understand4">관련 주요 질환</Link>
                                </div>
                            </li>
                            <li onClick={
                                ()=> {
                                    onClickMenu(3);
                                }
                            }>
                                {
                                    menuNo === 3 ?<KeyboardArrowDownIcon/>:<KeyboardArrowRightIcon/>
                                }
                                사전 서비스 단계
                                <div className={menuNo===3?'sub-menu-open':'sub-menu'}>
                                    <Link href="/content/advance1">서비스 대상 가구 선정</Link><br/>
                                    <Link href="/content/advance2">서비스 준비 절차</Link><br/>
                                    <Link href="/content/advance3">사전 교육 및 안전관리</Link><br/>
                                    <Link href="/content/advance4">사전 준비물 점검</Link>
                                </div>
                            </li>
                            <li onClick={
                                ()=> {
                                    onClickMenu(4);
                                }
                            }>
                                {
                                    menuNo === 4 ?<KeyboardArrowDownIcon/>:<KeyboardArrowRightIcon/>
                                }
                                서비스 진행 단계
                                <div className={menuNo===4?'sub-menu-open':'sub-menu'}>
                                    <Link href="/content/progress1">서비스 유형별 절차</Link><br/>
                                    <Link href="/content/progress2">서비스 제공 단계별 실행 가이드</Link><br/>
                                    <Link href="/content/progress3">폐기물 처리</Link><br/>
                                    <Link href="/content/progress4">주거청소</Link><br/>
                                    <Link href="/content/progress5">소독·방역</Link><br/>
                                    <Link href="/content/progress6">상황 발생에 대한 예방 및 관리대책</Link><br/>
                                </div>
                            </li>
                            <li onClick={
                                ()=> {
                                    onClickMenu(5);
                                }
                            }>
                                {
                                    menuNo === 5 ?<KeyboardArrowDownIcon/>:<KeyboardArrowRightIcon/>
                                }
                                사후 서비스 단계
                                <div className={menuNo===5?'sub-menu-open':'sub-menu'}>
                                    <Link href="/content/after1">대상가구 교육 및 입주 관리</Link><br/>
                                    <Link href="/content/after2">서비스 종결 점검</Link> <br/>
                                    <Link href="/content/after3">서비스 중단 및 재발에 대한 재개입 서비스</Link><br/>
                                    <Link href="/content/after4">사후 지원 서비스</Link><br/>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </Drawer>
            {/* 드로우 메뉴 끝 */}
            {
                /*드로우 메뉴를 열때 배경*/
                open?
                    <div className='menu-background' onClick={onClickMenuClose}></div>:null
            }
        </div>
    )
}

export default MobileMenuComponent;

