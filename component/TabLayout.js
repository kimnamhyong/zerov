import React from "react";

const TabLayout = ({index_no, onClickTab, tabMenu}) => {
    return (
        <>
            <div className='tab-menu'>
                <ul>
                    {
                        tabMenu.map((tab) => {
                            return (
                                <li onClick={() => {
                                    onClickTab(tab.id)
                                }}
                                className={index_no===tab.id?'active':''}
                                key={tab.id}
                                >
                                    {tab.menu}
                                </li>)
                        })
                    }
                </ul>
            </div>
        </>
    )
}
export default TabLayout;