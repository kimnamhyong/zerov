import React, {useCallback, useRef, useState} from "react";
import {Button, FormControl, InputAdornment, InputLabel, OutlinedInput, TextField} from "@mui/material";
import {Visibility, VisibilityOff} from "@mui/icons-material";
import IconButton from "@mui/material/IconButton";
import useInput from "../../hooks/useInput";
import {autoHyphone} from "../../util/function";
import {errorAlert, successAlertUrl} from "../../util/sweetalert2";
import {useRouter} from "next/router";

const SignupFormComponent = ({fadeInSlide}) => {
    const router = useRouter();
    //useState와 이벤트 설정하기
    const [user_id,onChangeUserId,setUserId] = useInput('');
    const [user_password,onChangeUserPassword,setUserPassword] = useInput('');
    const [user_password2,onChangeUserPassword2,setUserPassword2] = useInput('');
    const [user_name,onChangeUserName,setUserName] = useInput('');
    const [user_email,onChangeUserEmail,setUserEmail] = useInput('');
    //useRef 설정
    const userIdRef = useRef();
    const userPasswordRef = useRef();
    const userPassword2Ref = useRef();
    const userNameRef = useRef();
    const userEmailRef = useRef();


    //useState와 useCallback을 같이 불러오기 위한 것
    const [user_tel,onChangeUserTel,setUserTel] = useInput('');
    const [user_tel_No,setUserTelNo] = useState('');

    const [showPassword,setShowPassword] = useState([false,false]);//비밀번호 타입 변경할 때 필요
    const onClickSignUp = useCallback(()=>{
        if(user_id.length < 4) {
            errorAlert(userIdRef,'회원아이디 입력 오류','아이디는 4글자 이상 입력하셔야 합니다');
            return;
        }
        if(user_password.length < 8) {
            errorAlert(userPasswordRef,'비밀번호 입력 오류','비밀번호는 8글자 이상 입력하셔야 합니다');
            return;
        }
        if(user_password !== user_password2){
            errorAlert(userPassword2Ref,'비밀번호 재확인 오류', '비밀번호를 재확인하십시오');
            return;
        }
        if(user_name.length < 2){
            errorAlert(userNameRef,'이름 입력 오류','이름은 2글자 이상 입력하셔야 합니다');
            return;
        }
        if(user_email.length === 0){
            errorAlert(userEmailRef,'이메일 입력 입력','이메일을 입력하세요');
            return;
        }
        if(user_email.lastIndexOf('@') < 0){
            errorAlert(userEmailRef,'이메일 입력 입력','이메일 형식이 맞지 않습니다');
            return;
        }
        if(user_email.lastIndexOf('.') < 0){
            errorAlert(userEmailRef,'이메일 입력 입력','이메일 형식이 맞지 않습니다');
            return;
        }
        successAlertUrl('가입완료','축하드립니다\n회원가입이 되었습니다','/');
    },[user_id,user_password,user_password2,user_name,user_tel,user_email]);
    //type을 password로 할건지 아니면 text로 할건지
    const onClickShowPassword = (no) => {
        console.log(no);
        if(no === 0){
            console.log(showPassword[0]);
            setShowPassword([!showPassword[0],showPassword[1]]);
        }else{
            setShowPassword([showPassword[0],!showPassword[1]]);
        }
    }
    return (
        <div className={fadeInSlide}>
            <div className='titles'>회원가입</div>
            <form>
                <TextField
                    label="회원아이디"
                    variant="outlined"
                    fullWidth className='input'
                    inputRef={userIdRef}
                    onChange={onChangeUserId}
                />
                {/* 비밀번호 */}
                <FormControl fullWidth variant="outlined"  className='input'>
                    <InputLabel htmlFor="outlined-adornment-password">비밀번호</InputLabel>
                    <OutlinedInput
                        id="outlined-adornment-password"
                        fullWidth
                        type={showPassword[0] ? 'text' : 'password'}
                        onChange={onChangeUserPassword}
                        inputRef={userPasswordRef}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={()=>{
                                        onClickShowPassword(0)
                                    }}
                                    edge="end"
                                >
                                    {showPassword[0] ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                        label="Password"
                    />
                </FormControl>
                <FormControl fullWidth variant="outlined"  className='input'>
                    <InputLabel htmlFor="outlined-adornment-password">비밀번호 재확인</InputLabel>
                    <OutlinedInput
                        id="outlined-adornment-password"
                        fullWidth
                        onChange={onChangeUserPassword2}
                        inputRef={userPassword2Ref}
                        type={showPassword[1] ? 'text' : 'password'}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={()=>{
                                        onClickShowPassword(1)
                                    }}
                                    edge="end"
                                >
                                    {showPassword[1] ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                        label="Password"
                    />
                </FormControl>
                <TextField label="이름"
                           variant="outlined"
                           fullWidth
                           inputRef={userNameRef}
                           onChange={onChangeUserName}
                           className='input'/>
                <TextField label="연락처"
                           type="tel"
                           variant="outlined"
                           value={user_tel}
                           onChange={(e) => {
                               autoHyphone(e,setUserTel);
                           }}
                           fullWidth
                           className='input'/>
                <TextField label="이메일"
                           type="email"
                           variant="outlined"
                           inputRef={userEmailRef}
                           onChange={onChangeUserEmail}
                           fullWidth
                           className='input'/>
                <div className='confirm-btns'>
                    <Button variant='contained' size='large' onClick={onClickSignUp}>가입하기</Button>
                </div>
            </form>
        </div>
    )
}

export default SignupFormComponent;