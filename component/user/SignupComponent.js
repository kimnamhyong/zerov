import React, {useState} from "react";
import SignupAgreeComponent from "../../component/user/SignupAgreeComponent";
import SignupFormComponent from "../../component/user/SignupFormComponent";

const SignupComponent = () => {
    const [fadeInSlide,setFadeInSlide] = useState('no-animation');
    const [fadeOutSlide,setFadeOutSlide] = useState('')
    const onClickFadeInSlide = ()=>{
        setFadeOutSlide('fadeOut-down');//동의하기를 사라지게 하기
        setInterval(()=>{
            setFadeInSlide('fadeIn-up ');//회원가입 컴포넌트 보이게 하기
            setFadeOutSlide(...fadeOutSlide,'no-animation');//동의하기 컴포넌트 사라지게 하기
        },500);
    }
    return (
        <div>
            {/* 이용약관 동의하기 컴포넌트*/}
            <SignupAgreeComponent
                onClickFadeInSlide={onClickFadeInSlide}
                fadeOutSlide={fadeOutSlide}/>
            {/* 회원가입 동의하기 컴포넌트 */}
            <SignupFormComponent
                fadeInSlide={fadeInSlide}
            />
        </div>
    );
}

export default SignupComponent;