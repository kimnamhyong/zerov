import React, {useCallback, useRef, useState} from "react";
import {Button, Checkbox, FormControlLabel, FormGroup} from "@mui/material";
import nl2br from "react-nl2br";
import {agreeContent1, agreeContent2} from "../../util/variable";
import {errorAlert} from "../../util/sweetalert2";

const SignupAgreeComponent = ({onClickFadeInSlide,fadeOutSlide}) => {
    const checkRef = useRef();
    const [is_checked,setIsChecked] = useState([false,false]);
    //전체 동의하기 이벤트
    const onClickChecked = () => {
        setIsChecked([checkRef.current.checked,checkRef.current.checked]);
    }
    //이용약관 동의하기 이벤트
    const onClickChecked1 = (e) => {
        setIsChecked([e.target.checked,is_checked[1]]);
    }
    //개인정보처리 방침 이벤트
    const onClickChecked2 = (e) => {
        setIsChecked([is_checked[0],e.target.checked]);
    }
    //가입하기 버튼을 눌렀을 때
    const onClickSignUp = useCallback(()=>{
        console.log(is_checked);
        if(is_checked[0] === false){
            errorAlert(null,'이용약관 동의','이용약관을 동의하셔야 합니다');
            return;
        }
        if(is_checked[1] === false){
            errorAlert(null,'개인정보 수집 및 이용동의','개인정보 수집 및 이용동의하셔야 합니다');
            return;
        }
        onClickFadeInSlide();
    },[is_checked,onClickFadeInSlide]);

    return (
        <div className={fadeOutSlide}>
            <div className='titles'>약관동의</div>
            <FormGroup>
                <FormControlLabel control={<Checkbox inputRef={checkRef} onClick={onClickChecked}/>} label="모두 동의" /><br/>
                <FormControlLabel required
                                  control={
                                    <Checkbox
                                        checked={is_checked[0]}
                                        onClick={onClickChecked1}
                                    />}
                                  label="이용약관 동의 (필수)" />
                <div className="agree-form">
                    {nl2br(agreeContent1)}
                </div>
                <FormControlLabel required
                                  control={
                                    <Checkbox checked={is_checked[1]}
                                    onClick={onClickChecked2}
                                    />
                                    }
                                  label="개인정보 수집 및 이용동의 (필수)" />
                <div className="agree-form">
                    {nl2br(agreeContent2)}
                </div>
            </FormGroup>
            <div className='confirm-btns'>
                <Button variant='outlined' color="error" size='large'>취소</Button> &nbsp;
                <Button variant='contained' size='large' onClick={onClickSignUp}>가입하기</Button>
            </div>
        </div>
    )
}

export default SignupAgreeComponent;