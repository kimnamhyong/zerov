const autoHyphone = (e,setter) => {
    let tel=e.target.value.replace(/[^0-9]/g, '');
    tel=tel.replace(/[-]/g,'');
    if(3 < tel.length){
        tel=tel.replace(/(\d{3})(\d{1})/g, '$1-$2');
    }else if(7 < tel.length){
        tel=tel.replace(/(\d{3})(\d{4})(\d{1})/g, '$1-$2-$3');
    }else if(11 < tel.length){
        tel=tel.replace(/(\d{3})(\d{4})(\d{4})/g, '$1-$2-$3');

    }
    tel=tel.substring(0,13);
    setter(tel);
}

export {autoHyphone}