import { useState, useCallback } from 'react';

export default (initValue = null) => {
  const [value, setter] = useState(initValue);
  const handler = useCallback((e) => {
    console.log(value);
    setter(e.target.value);
  }, []);
  return [value, handler, setter];
};
